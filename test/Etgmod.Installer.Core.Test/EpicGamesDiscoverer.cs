﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Etgmod.Installer.Core.Discovery;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NUnit.Framework;

namespace Etgmod.Installer.Core.Test
{
    public class EpicGamesDiscovererTest
    {
        private static readonly EtgPlatformSpecifics MockPlatformSpecifics = new EtgPlatformSpecifics("myexecutable", "myprocess");

        Mock<IFileSystem> _fileSystem;

        EpicGamesDiscoverer _discoverer;

        List<string> _testItems = new List<string>();

        [SetUp]
        public void BeforeEachTestSetup()
        {
            _fileSystem = new Mock<IFileSystem>();
            _fileSystem.Setup(f => f.DirectoryExists("TestingDirectory")).Returns(true);
            _fileSystem.Setup(f => f.EnumerateFiles("TestingDirectory", "*.item")).Returns(() => _testItems);

            _discoverer = new EpicGamesDiscoverer(_fileSystem.Object, MockPlatformSpecifics, NullLogger<EpicGamesDiscoverer>.Instance)
            {
                EpicGamesManifestPath = "TestingDirectory"
            };
            _discoverer.Initialize();
        }

        [Test]
        public void ShouldFindProperFile()
        {
            _testItems.Add("test.item");
            _fileSystem.Setup(f => f.FileReadAllText("test.item")).Returns(MockManifest());
            _fileSystem.Setup(f => f.FileExists("FakeInstallLocation\\myexecutable")).Returns(true);

            var info = _discoverer.DiscoverEtgInstallation();
            Assert.AreEqual("FakeInstallLocation\\myexecutable", info.Path.RawPath);
            Assert.AreEqual(EpicGamesDiscoverer.SourceName, info.DiscoverySource);
        }

        private string MockManifest()
        {
            return JsonSerializer.Serialize(new Dictionary<string, string>()
            {
                ["LaunchExecutable"] = "EtG.exe",
                ["CatalogItemId"] = EpicGamesDiscoverer.EnterTheGungeonCatalogItemId,
                ["InstallLocation"] = "FakeInstallLocation"
            });
        }
    }
}
