﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public class EtgPaths
    {
        private readonly ILogger _logger;

        public EtgPaths(ILogger<EtgPaths> logger)
        {
            _logger = logger;
            SetApplicationPath(new ApplicationPath(string.Empty));
        }

        public static string AssemblyCsharpDllFileName { get; } = "Assembly-CSharp.dll";

        public static string UnityEngineCoreModuleDllFileName { get; } = "UnityEngine.CoreModule.dll";

        /// <summary>
        /// The directory containing the executable, on windows this folder containing EtG.exe.
        /// Something like D:\Steam\steamapps\common\Enter the Gungeon
        /// </summary>
        public string GameDirectory { get; private set; }

        /// <summary>
        /// Enter the Gungeon\EtG_Data. On mac EtG_Data is not located in the spot.
        /// </summary>
        public string EtgDataDirectory { get; private set; }

        /// <summary>
        /// Enter the Gungeon\EtG_Data\Managed. In the old installer this was called MainModDir.
        /// </summary>
        public string ManagedDirectory { get; private set; }

        /// <summary>
        /// Enter the Gungeon\EtG_Data\Managed\Assembly-Csharp.dll
        /// </summary>
        public string InstalledAssemblyCsharpFile { get; private set; }

        /// <summary>
        /// Enter the Gungeon\EtG_Data\Managed\Assembly-Csharp.dll
        /// </summary>
        public string InstalledUnityEngineCoreModuleFile { get; private set; }

        /// <summary>
        /// Enter the Gungeon\EtG_Data\Managed\ModBackup
        /// </summary>
        public string ModBackupDirectory { get; private set; }

        /// <summary>
        /// Enter the Gungeon\EtG_Data\Managed\ModCache
        /// </summary>
        public string ModCacheDirectory { get; private set; }

        /// <summary>
        /// On Windows, something like Enter the Gungeon\EtG.exe
        /// On Linux, something like Enter the Gungeon/EtG.x86_64
        /// On Mac, something like Enter the Gungeon/EtG_OSX.app/Contents/MacOS/EtG_OSX
        /// </summary>
        public string CoreExecutableFile { get; private set; }

        /// <summary>
        /// On Windows, something like Enter the Gungeon\UnityPlayer.dll
        /// On Linux, something like Enter the Gungeon/EtG.x86_64
        /// On Mac, something like Enter the Gungeon/EtG_OSX.app/Contents/MacOS/EtG_OSX
        /// </summary>
        public string PatchTargetFile { get; private set; }

        public void SetApplicationPath(ApplicationPath appPath)
        {
            if (appPath == null)
                throw new ArgumentNullException(nameof(appPath));

            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                // mac has application bundles ending with .app which are often treated as "apps" rather than folders.
                // accomodate/detect this and handle it accordingly.
                string contentsDir;
                if (appPath.RawPath.EndsWith(".app"))
                {
                    contentsDir = Path.Combine(appPath.RawPath, "Contents");
                    CoreExecutableFile = Path.Combine(contentsDir, "MacOS", "EtG_OSX");
                    GameDirectory = Path.GetDirectoryName(appPath.RawPath) ?? string.Empty;
                }
                else
                {
                    // assume we are given the executable, like EtG_OSX
                    string macOs = Path.GetDirectoryName(appPath.RawPath) ?? string.Empty;
                    contentsDir = Path.GetDirectoryName(macOs) ?? string.Empty;
                    string app = Path.GetDirectoryName(contentsDir) ?? string.Empty;
                    CoreExecutableFile = appPath.RawPath;
                    GameDirectory = Path.GetDirectoryName(app) ?? string.Empty;
                }

                EtgDataDirectory = Path.Combine(contentsDir, "Resources", "Data");
                ManagedDirectory = Path.Combine(EtgDataDirectory, "Managed");
            }
            else
            {
                CoreExecutableFile = appPath.RawPath;
                GameDirectory = Path.GetDirectoryName(appPath.RawPath) ?? string.Empty;
                EtgDataDirectory = Path.Combine(GameDirectory, "EtG_Data");
                ManagedDirectory = Path.Combine(EtgDataDirectory, "Managed");
            }
            
            ModBackupDirectory = Path.Combine(ManagedDirectory, "ModBackup");
            ModCacheDirectory = Path.Combine(ManagedDirectory, "ModCache");

            InstalledAssemblyCsharpFile = Path.Combine(ManagedDirectory, AssemblyCsharpDllFileName);
            InstalledUnityEngineCoreModuleFile = Path.Combine(ManagedDirectory, UnityEngineCoreModuleDllFileName);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                string exeDir = Path.GetDirectoryName(CoreExecutableFile) ?? string.Empty;
                PatchTargetFile = Path.Combine(exeDir, "UnityPlayer.dll");
            }
            else
            {
                PatchTargetFile = CoreExecutableFile;
            }
        }

        public bool Exists(IFileSystem fileSystem)
        {
            if (!fileSystem.FileExists(CoreExecutableFile))
            {
                return false;
            }

            if (!fileSystem.FileExists(PatchTargetFile))
            {
                return false;
            }

            if (!fileSystem.DirectoryExists(GameDirectory))
            {
                return false;
            }

            if (!fileSystem.DirectoryExists(EtgDataDirectory))
            {
                return false;
            }

            return true;
        }

        public void Log()
        {
            _logger.LogInformation("GameDirectory: {Path}", GameDirectory);
            _logger.LogInformation("EtG_Data: {Path}", EtgDataDirectory);
            _logger.LogInformation("Managed: {Path}", ManagedDirectory);
            _logger.LogInformation("Executable: {Path}", CoreExecutableFile);
            _logger.LogInformation("Patch target: {Path}", PatchTargetFile);
        }
    }
}
