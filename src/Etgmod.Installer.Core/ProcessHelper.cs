using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public class ProcessHelper : IProcessHelper
    {
        private readonly EtgPlatformSpecifics _platformSpecifics;
        private readonly ILogger _logger;

        public ProcessHelper(EtgPlatformSpecifics platformSpecifics, ILogger<ProcessHelper> logger)
        {
            _platformSpecifics = platformSpecifics;
            _logger = logger;
        }

        public void CloseEtgProcesses()
        {
            int runCount = 0;
            InvokeOnProcesses(p =>
            {
                runCount++;
                try
                {
                    _logger.LogInformation("Politely asking process {ProcessName} {Id} to exit", p.ProcessName, p.Id);
                    if (p.CloseMainWindow())
                    {
                        p.WaitForExit(5_000);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogInformation(e, "Exception calling CloseMainWindow on {ProcessName} {Id}", p.ProcessName, p.Id);
                }

                try
                {
                    p.Refresh();
                    if (!p.HasExited)
                    {
                        _logger.LogWarning("Killing process {ProcessName} {Id}", p.ProcessName, p.Id);
                        p.Kill();
                        Thread.Sleep(250);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, "Exception killing process {ProcessName} {Id}. Something bad might happen.", p.ProcessName, p.Id);
                }

                return true;
            }, _platformSpecifics.ProcessName);
        }

        public IEnumerable<(string processName, string mainModuleFile)> GetEtgProcesses()
        {
            var items = new List<(string processName, string mainModuleFile)>();
            InvokeOnProcesses(p =>
            {
                items.Add((p.ProcessName, p.MainModule.FileName));
                return true;
            }, _platformSpecifics.ProcessName);

            return items;
        }

        private void InvokeOnProcesses(Func<Process, bool> action, string processName)
        {
            Process[] processes = Process.GetProcessesByName(_platformSpecifics.ProcessName);
            foreach (var process in processes)
            {
                using (process)
                {
                    if (!action(process))
                        return;
                }
            }
        }
    }
}
