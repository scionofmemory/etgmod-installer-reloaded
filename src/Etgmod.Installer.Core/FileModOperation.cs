using System;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public abstract class FileModOperation
    {
        protected FileModOperation(IFileSystem fileSystem, ILogger logger)
        {
            FileSystem = fileSystem;
            Logger = logger;
        }

        public abstract string DisplayName { get; }

        protected IFileSystem FileSystem { get; }

        protected ILogger Logger { get; }

        public void Perform(EtgPaths paths)
        {
            PerformCore(paths);
        }

        protected abstract void PerformCore(EtgPaths paths);
    }
}
