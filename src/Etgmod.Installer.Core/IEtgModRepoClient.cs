using System.Threading.Tasks;

namespace Etgmod.Installer.Core
{
    public interface IEtgModRepoClient
    {
        Task<int> GetRevision();

        Task DownloadEtgModZipToFile(string outFile);
    }
}