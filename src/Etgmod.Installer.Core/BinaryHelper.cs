﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Etgmod.Installer.Core
{
    // I should feel bad for being too lazy to implement this on my own.
    // Source: http://stackoverflow.com/a/28607981
    public static class BinaryHelper
    {
        public static IEnumerable<byte> GetByteStream(BinaryReader reader)
        {
            const int BufferSize = 8192;
            byte[] buffer = new byte[BufferSize];

            while (true)
            {
                int read = reader.Read(buffer, 0, buffer.Length);
                if (read == 0)
                {
                    break;
                }

                for (int i = 0; i < read; i++)
                { 
                    yield return buffer[i];
                }
            }
        }

        public static void Replace(BinaryReader reader, BinaryWriter writer, IEnumerable<(byte[], byte[])> searchAndReplace)
        {
            foreach (byte d in Replace(GetByteStream(reader), searchAndReplace)) 
            { 
                writer.Write(d); 
            }
        }

        public static IEnumerable<byte> Replace(IEnumerable<byte> source, IEnumerable<(byte[], byte[])> searchAndReplace)
        {
            foreach (var (sourceData, destData) in searchAndReplace)
            {
                source = Replace(source, sourceData, destData);
            }

            return source;
        }

        public static IEnumerable<byte> Replace(IEnumerable<byte> input, IEnumerable<byte> from, IEnumerable<byte> to)
        {
            var fromEnumerator = from.GetEnumerator();
            fromEnumerator.MoveNext();
            int match = 0;
            foreach (var data in input)
            {
                if (data == fromEnumerator.Current)
                {
                    match++;
                    if (fromEnumerator.MoveNext()) { continue; }
                    foreach (byte d in to) { yield return d; }
                    match = 0;
                    fromEnumerator.Reset();
                    fromEnumerator.MoveNext();
                    continue;
                }

                if (0 != match)
                {
                    foreach (byte d in from.Take(match)) { yield return d; }
                    match = 0;
                    fromEnumerator.Reset();
                    fromEnumerator.MoveNext();
                }

                yield return data;
            }

            if (0 != match)
            {
                foreach (byte d in from.Take(match)) 
                { 
                    yield return d; 
                }
            }
        }

    }
}
