using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public class FileModOperationFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public FileModOperationFactory(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public PatchTargetBinary CreatePatchBinaryOperation()
        {
            var patchOp = _serviceProvider.GetRequiredService<PatchTargetBinary>();
            return patchOp;
        }

        public MonoModAssembly CreateMonoModAssemblyOperation(string assemblyFileName)
        {
            var modOp = _serviceProvider.GetRequiredService<MonoModAssembly>();
            modOp.AssemblyFileName = assemblyFileName;
            return modOp;
        }
    }
}
