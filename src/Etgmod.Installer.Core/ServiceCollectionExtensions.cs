﻿using System;
using Etgmod.Installer.Core.Discovery;
using Microsoft.Extensions.DependencyInjection;

namespace Etgmod.Installer.Core
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEtgmodInstallerServices(this IServiceCollection services)
        {
            services
                .AddSingleton(EtgPlatformSpecifics.Current)
                .AddScoped<IFileSystem, FileSystem>()
                .AddScoped<EtgPaths>()
                .AddScoped<IEtgModRepoClient, EtgModRepoClient>()
                .AddScoped<IModCacheManager, ModCacheManager>()
                .AddScoped<IModOperations, ModOperations>()
                .AddScoped<IDiscoveryService, DiscoveryService>()
                .AddScoped<IProcessHelper, ProcessHelper>()
                .AddScoped<EtgInstallationInfo>()
                .AddScoped<EtgModRepoClient>()
                .AddScoped<FileModOperationFactory>()
                .AddScoped<EtgModder>();

            services
                .ScanForImplementationsOfType(typeof(IDiscoverer))
                .ScanForImplementationsOfType(typeof(FileModOperation));

            services.AddHttpClient<EtgModRepoClient>(c =>
            {
                c.DefaultRequestHeaders.UserAgent.TryParseAdd("ETGMod Installer");
            });

            return services;
        }

        public static IServiceCollection AddLoggingReporter(this IServiceCollection services)
        {
            return services.AddSingleton<IReporter, LoggingReporter>();
        }

        private static IServiceCollection ScanForImplementationsOfType(this IServiceCollection services, Type serviceType)
        {
            foreach (var type in serviceType.Assembly.GetTypes())
            {
                if (type.IsAbstract || type.IsGenericTypeDefinition)
                    continue;

                if (serviceType.IsAssignableFrom(type))
                {
                    // NOTE: lifetime scoped does not seem to work here for some reason but that's fine for our cases.
                    services.Add(new ServiceDescriptor(type, type, ServiceLifetime.Transient));
                }
            }

            return services;
        }
    }
}
