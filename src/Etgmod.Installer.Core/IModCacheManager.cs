using System.Threading.Tasks;

namespace Etgmod.Installer.Core
{
    public interface IModCacheManager
    {
        Task<CachedModInfo> GetCachedInfo();

        Task SetRevision(int revision);
    }
}
