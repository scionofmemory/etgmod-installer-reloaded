﻿using MonoMod;
using Microsoft.Extensions.Logging;
using Mono.Cecil;

namespace Etgmod.Installer.Core
{
    public class LoggingMonoModder : MonoModder
    {
        private readonly IReporter _reporting;
        private readonly ILogger _logger;

        public LoggingMonoModder(IReporter reporting, ILogger logger)
        {
            _reporting = reporting;
            _logger = logger;
            
            LogVerboseEnabled = true;
            ReaderParameters.ReadSymbols = false;
            WriterParameters.WriteSymbols = false;
            WriterParameters.SymbolWriterProvider = null;
        }

        public override void Log(string text)
        {
            _reporting.OnActivity();
            _logger.LogInformation(text);
        }

        public override void LogVerbose(string text)
        {
            if (LogVerboseEnabled)
            {
                _logger.LogDebug(text);
            }
        }

        public override void ParseRules(ModuleDefinition mod)
        {
            // intentionally do nothing
            // This ignores MonoMod.Rules in the mm patch assemblies.
            // These were built with an older version of monomod causing
            // loading errors with the newer version we are using in the installer.
            // The Monomod.Rules in the patch assemblies are actually only examples 
            // and otherwise have no effect on the patched game.
            // We need a newer version of monomod for a bug fix that produces the 
            // correct references during the patching process, while running in .net core/.net 5.
        }
    }
}
