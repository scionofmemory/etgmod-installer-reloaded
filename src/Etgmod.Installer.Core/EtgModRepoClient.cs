using System;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public sealed class EtgModRepoClient : IEtgModRepoClient
    {
        public static readonly string EtgModZipName = "ETGMOD.zip";

        public static readonly string ETGModURL = $"http://modthegungeon.github.io/{EtgModZipName}";
        public static readonly string ETGModRevisionURL = "http://modthegungeon.github.io/ETGMOD_REVISION.txt";
        
        private readonly HttpClient _client;
        private readonly IReporter _reporting;
        private readonly IFileSystem _fileSystem;
        private readonly ILogger _logger;

        public EtgModRepoClient(
            HttpClient client, 
            IFileSystem fileSystem,
            IReporter reporting, 
            ILogger<EtgModRepoClient> logger)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _reporting = reporting ?? throw new ArgumentNullException(nameof(reporting));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            EtgModZipUrl = ETGModURL;
            EtgModRevisionUrl = ETGModRevisionURL;
        }

        public string EtgModZipUrl { get; set; }

        public string EtgModRevisionUrl { get; set; }

        public string RevisionFile { get; set; }
        
        public async Task<int> GetRevision()
        {
            try
            {
                var resp = await _client.GetAsync(EtgModRevisionUrl);
                string data = await resp.Content.ReadAsStringAsync();
                return int.Parse(data.Trim());
            }
            catch (Exception e)
            {
                _logger.LogDebug(e, "Could not get revision from repo");
                return int.MinValue;
            }
        }

        public async Task DownloadEtgModZipToFile(string outFile)
        {
            await Download(EtgModZipUrl, outFile);
        }

        private async Task Download(string url, string file)
        {
            _reporting.Log($"Downloading {url}...");
            _reporting.InitProgress("Starting download", 1);

            DateTime timeStart = DateTime.Now;
            using var response = await _client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead);
            response.EnsureSuccessStatusCode();
            if (response.Content == null)
            {
                _reporting.EndProgress("Download did not contain file");
                return;
            }

            long contentLength = response.Content.Headers.ContentLength ?? -1;
            long progressSize = contentLength;
            int progressScale = 1;
            while (progressSize > int.MaxValue)
            {
                progressScale *= 10;
                progressSize = contentLength / progressScale;
            }

            _reporting.InitProgress("Downloading", (int)progressSize);

            using var fs = _fileSystem.OpenFileStream(file, FileMode.Create);

            DateTime timeLast = timeStart;
            var contentStream = await response.Content.ReadAsStreamAsync();
            var buffer = new byte[2 * 4096];

            int speed = 0;
            int readForSpeed = 0;
            int pos = 0;
            while (true)
            {
                int read = contentStream.Read(buffer, 0, buffer.Length);
                if (read == 0)
                    break;

                fs.Write(buffer, 0, read);

                readForSpeed += read;
                pos += read;

                var delta = DateTime.Now - timeLast;
                if (delta.TotalMilliseconds > 100)
                {
                    speed = (int)(readForSpeed / 1024d / (double)delta.TotalSeconds);
                    readForSpeed = 0;
                    timeLast = DateTime.Now;
                }

                _reporting.SetProgress(
                    "Downloading - " +
                        (int)(Math.Round(100d * ((double)(pos / progressScale) / (double)progressSize))) + "%, " +
                        speed + " KiB/s",
                    pos / progressScale
                );
            }

            _reporting.EndProgress("Download complete");

            string logSize = (fs.Length / 1024D).ToString(CultureInfo.InvariantCulture);
            logSize = logSize.Substring(0, Math.Min(logSize.IndexOf('.') + 3, logSize.Length));
            string logTime = (DateTime.Now - timeStart).TotalSeconds.ToString(CultureInfo.InvariantCulture);
            logTime = logTime.Substring(0, Math.Min(logTime.IndexOf('.') + 3, logTime.Length));
            _reporting.Log($"Download complete, {logSize} KiB in {logTime} s.");
        }
    }
}