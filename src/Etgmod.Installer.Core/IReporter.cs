﻿namespace Etgmod.Installer.Core
{
    public interface IReporter
    {
        void Log(string message);

        void InitProgress(string message, int max);

        void ImportantProgressMessage(string message);

        void SetProgress(int value);

        void SetProgress(string message, int value);

        void EndProgress(string message);

        void OnActivity();
    }
}
