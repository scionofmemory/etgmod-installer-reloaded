using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using System.Runtime.InteropServices;

namespace Etgmod.Installer.Core
{
    public class PatchTargetBinary : FileModOperation
    {
        private readonly static List<(byte[], byte[])> NativeResourceReplacementMap = GenOrigReplacementMap(
            ("UnityEngine.Resources", "Load"),
            ("UnityEngine.Resources", "LoadAsync"),
            ("UnityEngine.Resources", "LoadAll"),
            ("UnityEngine.Resources", "GetBuiltinResource"),
            ("UnityEngine.Resources", "UnloadAsset"),
            ("UnityEngine.Resources", "UnloadUnusedAssets")
        );

        public PatchTargetBinary(IFileSystem fileSystem, ILogger<PatchTargetBinary> logger)
            : base(fileSystem, logger)
        {
        }

        public override string DisplayName => $"Patch Target";

        protected override void PerformCore(EtgPaths paths)
        {
            string inFileName = Path.GetFileName(paths.PatchTargetFile);
            string inFilePath = Path.Combine(paths.ModBackupDirectory, inFileName);
            string outFilePath = paths.PatchTargetFile;
            Logger.LogInformation("Patching {From} -> {To}", inFilePath, outFilePath);

            {
                using var fo = FileSystem.OpenFileStream(outFilePath, FileMode.Create);
                using var fi = FileSystem.OpenFileStream(inFilePath, FileMode.Open);
                using var bo = new BinaryWriter(fo);
                using var bi = new BinaryReader(fi);
                BinaryHelper.Replace(bi, bo, NativeResourceReplacementMap);
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                using var chmod = new Process();
                chmod.StartInfo.FileName = "chmod";
                chmod.StartInfo.Arguments = "a+x \"" + outFilePath + "\"";
                chmod.StartInfo.CreateNoWindow = true;
                chmod.StartInfo.UseShellExecute = false;
                chmod.Start();
                chmod.WaitForExit();
                chmod.Refresh();
                Logger.LogDebug("chmod exit code: {ExitCode}", chmod.ExitCode);
            }
        }

        private static List<(byte[], byte[])> GenOrigReplacementMap(params (string, string)[] sa)
        {
            var encoding = Encoding.ASCII;
            var results = new List<(byte[], byte[])>(sa.Length);
            foreach (var (className, methodName) in sa)
            {
                results.Add((
                    encoding.GetBytes(className + "::" + methodName),
                    encoding.GetBytes(className + "::O" + methodName[1..])
                ));
            }
            
            return results;
        }
    }
}
