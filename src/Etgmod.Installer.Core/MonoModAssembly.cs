using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public class MonoModAssembly : FileModOperation
    {
        private readonly IReporter _reporting;

        public MonoModAssembly(IFileSystem fileSystem, IReporter reporting, ILogger<MonoModAssembly> logger)
            : base(fileSystem, logger)
        {
            _reporting = reporting;
        }

        public string AssemblyFileName { get; set; }

        public override string DisplayName => $"Mod: {AssemblyFileName}";

        protected override void PerformCore(EtgPaths paths)
        {
            string inPath = Path.Combine(paths.ManagedDirectory, AssemblyFileName);
            string outPath = Path.Combine(paths.ManagedDirectory, Path.GetFileNameWithoutExtension(AssemblyFileName) + ".tmp.dll");
            Logger.LogInformation("Modding assembly start {Path}", inPath);

            // Unity wants .mdbs
            // monomod.WriterParameters.SymbolWriterProvider = new Mono.Cecil.Mdb.MdbWriterProvider();
            string db = Path.ChangeExtension(inPath, "pdb");
            string dbTmp = Path.ChangeExtension(outPath, "pdb");
            if (!FileSystem.FileExists(db))
            {
                db = inPath + ".mdb";
                dbTmp = outPath + ".mdb";
            }

            while (true)
            {
                try
                {
                    using (var monomod = new LoggingMonoModder(_reporting, Logger))
                    {
                        monomod.InputPath = inPath;
                        monomod.OutputPath = outPath;

                        monomod.Read(); // Read main module first
                        monomod.ReadMod(paths.ManagedDirectory); // ... then mods
                        monomod.MapDependencies(); // ... then all dependencies
                        monomod.AutoPatch(); // Patch,
                        monomod.Write(); // and write.
                    }

                    FileSystem.FileDelete(inPath);
                    FileSystem.FileMove(outPath, inPath);
                    FileSystem.EnsureFileDeleted(db);
                    if (FileSystem.FileExists(dbTmp))
                    {
                        FileSystem.FileMove(dbTmp, db);
                    }

                    Logger.LogInformation("Modding assembly finished {Path}", inPath);
                    return;
                }
                catch (ArgumentException e)
                {
                    if (FileSystem.FileExists(db))
                    {
                        Logger.LogDebug(e, "Argument exception is being retried");
                        FileSystem.FileDelete(db);
                        FileSystem.EnsureFileDeleted(dbTmp);

                        continue;
                    }

                    Logger.LogWarning(e, "Argument exception terminating error during modding");
                    _reporting.Log(e.ToString());
                    throw;
                }
                catch (Exception e)
                {
                    Logger.LogWarning(e, "Other exception terminating error during modding");
                    _reporting.Log(e.ToString());
                    throw;
                }
            }
        }
    }
}
