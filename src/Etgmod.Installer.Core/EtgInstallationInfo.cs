using System;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public class EtgInstallationInfo
    {
        private readonly IFileSystem _fileSystem;
        private readonly EtgPaths _paths;
        private readonly ILogger _logger;

        private string _version;

        public EtgInstallationInfo(
            IFileSystem fileSystem,
            EtgPaths paths,
            ILogger<EtgInstallationInfo> logger)
        {
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _paths = paths ?? throw new ArgumentNullException(nameof(paths));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public string GetInstalledGameVersion()
        {
            if (_version == null)
            {
                string path = Path.Combine(_paths.EtgDataDirectory, "StreamingAssets", "version.txt");
                if (_fileSystem.FileExists(path))
                {
                    _version = _fileSystem.FileReadAllText(path).Trim();
                }
            }

            return _version;
        }
    }
}
