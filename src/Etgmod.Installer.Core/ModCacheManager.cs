using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public sealed class ModCacheManager : IModCacheManager
    {
        private readonly IFileSystem _fileSystem;
        private readonly EtgPaths _paths;
        private readonly ILogger _logger;

        public ModCacheManager(IFileSystem fileSystem, EtgPaths paths, ILogger<ModCacheManager> logger)
        {
            _fileSystem = fileSystem;
            _paths = paths;
            _logger = logger;

        }

        public static readonly string DefaultRevisionFileName = "ETGMOD_REVISION.txt";

        public static readonly string DefaultEtgModZipFileName = "ETGMOD.zip";

        public string RevisionFileName { get; set; } = DefaultRevisionFileName;

        public string EtgModZipFileName { get; set; } = DefaultEtgModZipFileName;

        public async Task<CachedModInfo> GetCachedInfo()
        {
            int? revision = await ReadRevisionFromFile();
            var (file, hash) = ComputeZipHash();
            return new CachedModInfo()
            {
                FilePath = file,
                Revision = revision,
                Sha256Hash = hash
            };
        }

        public async Task SetRevision(int revision)
        {
            string revisionFile = Path.Combine(_paths.ModCacheDirectory, RevisionFileName);
            if (_fileSystem.FileExists(revisionFile))
            {
                _fileSystem.FileDelete(revisionFile);
            }

            await _fileSystem.FileWriteAllTextAsync(revisionFile, revision.ToString());
        }

        private async Task<int?> ReadRevisionFromFile()
        {
            string revisionFile = Path.Combine(_paths.ModCacheDirectory, RevisionFileName);
            if (string.IsNullOrEmpty(revisionFile) || !_fileSystem.FileExists(revisionFile))
            {
                return null;
            }

            try
            {
                string contents = await _fileSystem.FileReadAllTextAsync(revisionFile);
                return int.Parse(contents.Trim());
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "Error reading revision file {Path}", revisionFile);
                return null;
            }
        }

        private (string, string) ComputeZipHash()
        {
            string modZip = Path.Combine(_paths.ModCacheDirectory, EtgModZipFileName);
            if (string.IsNullOrEmpty(modZip) || !_fileSystem.FileExists(modZip))
            {
                return (null, null);
            }

            try
            {
                using var fs = _fileSystem.OpenFileStream(modZip, FileMode.Open);
                using var sha2 = SHA256.Create();
                var hash = sha2.ComputeHash(fs);

                var hashStringBuilder = new StringBuilder();
                foreach (byte b in hash)
                {
                    hashStringBuilder.Append(b.ToString("x2"));
                }

                string hashString = hashStringBuilder.ToString();

                return (modZip, hashString);
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "Error computing zip hash {Path}", modZip);
                return (modZip, null);
            }
        }
    }
}
