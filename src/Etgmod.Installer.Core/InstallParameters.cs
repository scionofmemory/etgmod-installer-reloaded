﻿namespace Etgmod.Installer.Core
{
    public class InstallParameters
    {
        /// If true, EtG will be started after installation completes
        public bool AutoRun { get; set; } = false;

        public SourceOption Source { get; set; } = SourceOption.AutomaticDownload;

        /// If true the installer will force EtG processes to exit before executing.
        public bool ForceClose { get; set; } = false;

        /// Path to ETGMOD.zip that is used when the source is Zip.
        public string PackageZipFilePath { get; set; }

        public bool IsOffline => Source == SourceOption.Offline;
    }
}
