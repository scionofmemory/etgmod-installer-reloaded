namespace Etgmod.Installer.Core
{
    public class CachedModInfo
    {
        public string FilePath { get; set; }

        public int? Revision { get; set; }

        public string Sha256Hash { get; set;}
    }
}
