using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Runtime.InteropServices;

namespace Etgmod.Installer.Core
{
    public sealed class EtgModder
    {
        private readonly IReporter _reporting;
        private readonly IModOperations _ops;
        private readonly IFileSystem _fs;
        private readonly IProcessHelper _processHelper;
        private readonly IEtgModRepoClient _repoHelper;
        private readonly IModCacheManager _cacheManager;
        private readonly EtgPlatformSpecifics _platformSpecifics;
        private readonly EtgPaths _paths;
        private readonly EtgInstallationInfo _installationInfo;
        private readonly List<FileModOperation> _fileModOperations;
        private readonly ILogger _logger;

        public EtgModder(
            IModOperations ops,
            IReporter reporter,
            IFileSystem fileSystem,
            IDiscoveryService discoService,
            IProcessHelper processHelper,
            IEtgModRepoClient repoHelper,
            IModCacheManager cacheManager,
            EtgPlatformSpecifics platformSpecifics,
            EtgPaths paths,
            EtgInstallationInfo installationInfo,
            FileModOperationFactory modOperationFactory,
            ILogger<EtgModder> logger)
        {
            _ops = ops ?? throw new ArgumentNullException(nameof(ops));
            _reporting = reporter ?? throw new ArgumentNullException(nameof(reporter));
            _fs = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _processHelper = processHelper ?? throw new ArgumentNullException(nameof(processHelper));
            _repoHelper = repoHelper ?? throw new ArgumentNullException(nameof(repoHelper));
            _cacheManager = cacheManager ?? throw new ArgumentNullException(nameof(cacheManager));
            _platformSpecifics = platformSpecifics ?? throw new ArgumentNullException(nameof(platformSpecifics));
            _paths = paths ?? throw new ArgumentNullException(nameof(paths));
            _installationInfo = installationInfo ?? throw new ArgumentNullException(nameof(installationInfo));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            if (modOperationFactory == null)
            {
                throw new ArgumentNullException(nameof(modOperationFactory));
            }

            _fileModOperations = CreateFileModOperations(modOperationFactory);
        }

        public async Task<GameInstallationInfo> GetInfo()
        {
            try
            {
                _paths.Log();
                var info = new GameInstallationInfo()
                {
                    GameDirectory = _paths.GameDirectory
                };

                if (!_paths.Exists(_fs))
                {
                    return info;
                }

                info.Found = true;
                var cacheInfo = await _cacheManager.GetCachedInfo();
                info.CacheRevision = cacheInfo.Revision;
                info.CachedSha2Hash = cacheInfo.Sha256Hash;
                info.InstalledGameVersion = _installationInfo.GetInstalledGameVersion();
                string modVersion = _ops.GetModVersion(_paths.InstalledAssemblyCsharpFile);
                info.InstalledModVersion = modVersion;

                return info;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetInfo failed with unknown exception");
                _reporting.Log($"GetInfo had an unexpected error: {e.GetType().Name} {e.Message}");
                throw;
            }
        }

        public async Task Install(InstallParameters settings)
        {
            try
            {
                await InstallCore(settings);
            }
            catch (InstallationFailedException e)
            {
                _logger.LogInformation(e, "Install failed with internal exception");
                _reporting.Log($"Install failed: {e.Message}");
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Install failed with unknown exception");
                _reporting.Log($"Install had an unexpected error: {e.GetType().Name} {e.Message}");
                throw;
            }
        }

        public Task Uninstall(UninstallParameters parameters)
        {
            try
            {
                _paths.Log();
                if (!_paths.Exists(_fs))
                {
                    throw new InstallationFailedException("Paths were not valid");
                }

                _reporting.Log("Starting uninstall.");
                if (parameters.ForceClose)
                {
                    _processHelper.CloseEtgProcesses();
                }

                _ops.RestoreFromBackup();
                // TODO: There used to be a consistency check to see if monomod 
                // could still load the reverted dlls. Do We need that?
                _ops.ClearCache();
                _reporting.Log("Uninstall complete.");

                return Task.CompletedTask;
            }
            catch (InstallationFailedException e)
            {
                _logger.LogInformation(e, "Uninstall failed with internal exception");
                _reporting.Log($"Uninstall failed: {e.Message}");
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Uninstall failed with unknown exception");
                _reporting.Log($"Uninstall had an unexpected error: {e.GetType().Name} {e.Message}");
                throw;
            }
        }

        public void ClearSymbols(string path)
        {
            string[] files = _fs.GetFiles(path);
            _reporting.InitProgress("Clearing symbols", files.Length + 1);
            for (int i = 0; i < files.Length; i++)
            {
                string file = Path.GetFileName(files[i]);
                if (!file.EndsWith(".pdb") && !file.EndsWith(".mdb"))
                {
                    continue;
                }
                _reporting.Log($"Removing: {file}");
                _reporting.SetProgress("Removing: " + file, i);
                _fs.FileDelete(files[i]);
            }

            _reporting.EndProgress("Clearing symbols complete.");
        }

        private async Task InstallCore(InstallParameters settings)
        {
            _paths.Log();
            if (!_paths.Exists(_fs))
            {
                throw new InstallationFailedException("Paths were not valid");
            }

            _reporting.Log("Entering the Modgeon");
            if (settings.ForceClose)
            {
                _processHelper.CloseEtgProcesses();
            }

            // Clean the game from any previous installation
            string version = _installationInfo.GetInstalledGameVersion();
            string lastRunVersion = null; // TODO read some serializaed settings
            if (version != lastRunVersion && lastRunVersion != null)
            {
                _ops.ClearBackup();
            }
            else
            {
                // this is a typical case for already installed.
                _ops.RestoreFromBackup();
            }

            _ops.Backup();

            // Setup the files and MonoMod instances
            string zipFile = Path.Combine(_paths.ModCacheDirectory, "ETGMOD.zip");
            if (!string.IsNullOrEmpty(settings.PackageZipFilePath))
            {
                zipFile = settings.PackageZipFilePath;
                _reporting.Log($"Using ETGMOD.zip from settings: {zipFile}");
            }
            else if (settings.IsOffline)
            {
                if (!_fs.FileExists(zipFile))
                {
                    throw new InstallationFailedException($"Cache file '{zipFile}' was not found and offline option was specified.");
                }
            }
            else
            {
                _reporting.Log("Restoring Mod the Gungeon package");

                // Check if the revision online is newer
                _fs.EnsureDirectoryExists(_paths.ModCacheDirectory);
                bool toDownload = true;
                int onlineRevision = await _repoHelper.GetRevision();
                if (_fs.FileExists(zipFile))
                {
                    var cachedInfo = await _cacheManager.GetCachedInfo();
                    if (cachedInfo.Revision == onlineRevision)
                    {
                        _reporting.Log("Found matching version of ETGMOD.zip in cache");
                        toDownload = false;
                    }
                }

                if (toDownload)
                {
                    _fs.EnsureFileDeleted(zipFile);
                    try
                    {
                        await _repoHelper.DownloadEtgModZipToFile(zipFile);
                    }
                    catch (Exception e)
                    {
                        throw new InstallationFailedException("Failed to download ETGMOD.zip", e);
                    }

                    if (_fs.FileExists(zipFile))
                    {
                        _reporting.Log($"Downloaded to:{zipFile}");
                    }
                    else
                    {
                        throw new InstallationFailedException("Failed to download ETGMOD.zip");
                    }

                    await _cacheManager.SetRevision(onlineRevision);
                }
                else
                {
                    _reporting.Log($"Using cache: {zipFile}");
                }
            }

            using var fileStream = _fs.OpenFileStream(zipFile, FileMode.Open);
            if (!_ops.UnzipMod(fileStream))
            {
                throw new InstallationFailedException($"Failed to unzip {zipFile}");
            }

            _reporting.Log(Environment.NewLine);
            _reporting.Log("Now comes the real \"modding\" / patching process.");
            _reporting.Log("It may seem like the Installer may be stuck sometimes. Go make");
            _reporting.Log("yourself a coffee in the meantime - it doesn't get stuck.");
            _reporting.Log(Environment.NewLine);

            foreach (var operation in _fileModOperations)
            {
                var watch = Stopwatch.StartNew();
                _logger.LogInformation("Starting operation {Name}", operation.DisplayName);
                operation.Perform(_paths);
                watch.Stop();
                _logger.LogInformation("Operation {Name} complete. Elapsed:{Elapsed}", operation.DisplayName, watch.Elapsed);
            }

            _reporting.Log("Mod installation complete!");

            if (settings.AutoRun)
            {
                Process etg = new Process();
                etg.StartInfo.FileName = _paths.CoreExecutableFile;
                etg.Start();
            }
        }

        private List<FileModOperation> CreateFileModOperations(FileModOperationFactory opFactory)
        {
            var ops = new List<FileModOperation>()
            {
                opFactory.CreatePatchBinaryOperation(),
                opFactory.CreateMonoModAssemblyOperation(EtgPaths.UnityEngineCoreModuleDllFileName),
                opFactory.CreateMonoModAssemblyOperation(EtgPaths.AssemblyCsharpDllFileName),
            };

            return ops;
        }
    }
}
