﻿using System.IO;
using System.Threading.Tasks;

namespace Etgmod.Installer.Core
{
    public interface IModOperations
    {
        void Backup();

        void ClearBackup();

        void RestoreFromBackup();

        void ClearCache();

        bool UnzipMod(Stream zipStream);

        string GetModVersion(string assemblyCsharpPath);
    }
}
