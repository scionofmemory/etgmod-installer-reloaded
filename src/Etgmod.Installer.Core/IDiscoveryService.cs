namespace Etgmod.Installer.Core
{
    public interface IDiscoveryService
    {
        void Initialize();

        DiscoveryInformation DiscoverEtgInstallation();
    }
}