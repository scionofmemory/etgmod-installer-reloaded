﻿using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public sealed class LoggingReporter : IReporter
    {
        private readonly ILogger _logger;

        public LoggingReporter(ILogger<LoggingReporter> logger)
        {
            _logger = logger;
        }

        public void ImportantProgressMessage(string message)
        {
            _logger.LogWarning($"Important Message: {message}");
        }

        public void InitProgress(string message, int max)
        {
            _logger.LogInformation("Progress: {Message}, {Max} parts", message, max);
        }

        public void EndProgress(string message)
        {
            _logger.LogInformation("Complete: {Message}", message);
        }

        public void Log(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
                _logger.LogInformation(message);
        }

        public void OnActivity()
        {
            // not necessary
        }

        public void SetProgress(int value)
        {
        }

        public void SetProgress(string message, int value)
        {
        }
    }
}
