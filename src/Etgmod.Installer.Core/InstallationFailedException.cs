using System;
using System.Runtime.Serialization;

namespace Etgmod.Installer.Core
{
    [Serializable]
    public sealed class InstallationFailedException : Exception
    {
        public InstallationFailedException()
        {
        }

        public InstallationFailedException(string message) : base(message)
        {
        }

        public InstallationFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}