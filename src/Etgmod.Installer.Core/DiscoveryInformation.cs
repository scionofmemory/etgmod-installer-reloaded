namespace Etgmod.Installer.Core
{
    public class DiscoveryInformation
    {
        public DiscoveryInformation(ApplicationPath path, string source) 
        {
            Path = path;
            DiscoverySource = source;
        }

        public static DiscoveryInformation MissingApplication { get; } = new DiscoveryInformation(null, null);

        public bool Found => Path != null;

        public ApplicationPath Path { get; }

        public string DiscoverySource { get; }

        public override string ToString()
        {
            if (Found)
            {
                return $"Discovery: Path:{Path}, Source:{DiscoverySource}";
            }
            else
            {
                return "Discovery: MissingApp";
            }
        }
    }
}
