using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Etgmod.Installer.Core.Discovery;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public class DiscoveryService : IDiscoveryService
    {
        private readonly List<(string, IDiscoverer)> _discoverers;
        private readonly ILogger _logger;

        public DiscoveryService(
            IServiceProvider serviceProvider,
            ILogger<DiscoveryService> logger)
        {
            _discoverers = new List<(string, IDiscoverer)>();
            _logger = logger;

            foreach (var discoType in typeof(IDiscoverer).Assembly.GetTypes())
            {
                if (discoType.IsAbstract || discoType.IsInterface || discoType.IsGenericTypeDefinition)
                    continue;

                if (!typeof(IDiscoverer).IsAssignableFrom(discoType))
                    continue;

                try
                {
                    var discoverer = (IDiscoverer)serviceProvider.GetService(discoType);
                    if (discoverer == null)
                    {
                        _logger.LogWarning("Discoverer was not resolved by the serivce provider: {Type}", discoType);
                        continue;
                    }

                    string envVarName = $"ETGMODINSTALLER_{discoverer.Name}_ENABLED".ToUpperInvariant();
                    bool enabled = GetEnvVarEnabled(envVarName);
                    _logger.LogInformation("Discoverer {Name}: {Status}", discoverer.Name, enabled ? "enabled" : "disabled");
                    if (!enabled)
                    {
                        continue;
                    }

                    if (discoverer.SupportedPlatforms.Any(p => RuntimeInformation.IsOSPlatform(p)))
                    {
                        _discoverers.Add((discoverer.Name, discoverer));
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex, "Could not construct discoverer {Type}", discoType);
                }
            }

            if (_discoverers.Count == 0)
            {
                _logger.LogWarning("No discoverers were added");
            }
        }

        public void Initialize()
        {
            _logger.LogDebug("Initializing discoverer: {Count}", _discoverers.Count);
            foreach (var (name, discoverer) in _discoverers)
            {
                _logger.LogDebug("Initializing {Name} discoverer", name);
                discoverer.Initialize();
            }
        }

        public DiscoveryInformation DiscoverEtgInstallation()
        {
            foreach (var (name, discoverer) in _discoverers)
            {
                try
                {
                    _logger.LogDebug("Locating EtG by {Name}", name);
                    var discoInfo = discoverer.DiscoverEtgInstallation();
                    if (discoInfo.Found)
                    {
                        _logger.LogInformation("Found EtG by {Name} at {Path}", name, discoInfo);
                        return discoInfo;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, "Discoverer {Name} had an internal error", name);
                }
            }

            _logger.LogInformation("Did not find EtG");
            return DiscoveryInformation.MissingApplication;
        }

        private static bool GetEnvVarEnabled(string envVarName)
        {
            string envVarEnable = Environment.GetEnvironmentVariable(envVarName);
            if (envVarEnable == null)
            {
                return true;
            }

            if (string.Equals(envVarEnable, "0", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(envVarEnable, "false", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return true;
        }
    }
}