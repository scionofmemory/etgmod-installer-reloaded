namespace Etgmod.Installer.Core
{
    public sealed class GameInstallationInfo
    {
        public bool Found { get; set; }

        public string GameDirectory { get; set; }

        public string InstalledGameVersion { get;set; }

        public bool IsModded => !string.IsNullOrEmpty(InstalledModVersion);

        public string InstalledModVersion { get; set; }

        public int? CacheRevision { get; set; }

        public string CachedSha2Hash { get; set; }
    }
}
