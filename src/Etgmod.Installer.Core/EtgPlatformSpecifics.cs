using System;
using System.Runtime.InteropServices;

namespace Etgmod.Installer.Core
{
    public class EtgPlatformSpecifics
    {
        public EtgPlatformSpecifics(string executableName, string processName)
        {
            ExecutableName = executableName;
            ProcessName = processName;
        }

        public static EtgPlatformSpecifics Current { get; } = Create(GetOSPlatform(), RuntimeInformation.OSArchitecture);

        public string ExecutableName { get; }

        public string ProcessName { get; }

        public static EtgPlatformSpecifics Create(OSPlatform osPlatform, Architecture arch)
        {
            string exeName = GetExecutableName(osPlatform, arch);
            string procName = GetProcessName(osPlatform, arch);
            return new EtgPlatformSpecifics(exeName, procName);
        }

        private static OSPlatform GetOSPlatform()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return OSPlatform.Windows;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return OSPlatform.OSX;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                return OSPlatform.Linux;
            }

            throw new NotSupportedException($"OS not supported: {RuntimeInformation.OSDescription}");
        }
        
        private static string GetExecutableName(OSPlatform osPlatform, Architecture arch)
        {
            if (osPlatform == OSPlatform.Windows)
            {
                return "EtG.exe";
            }
            else if (osPlatform == OSPlatform.OSX)
            {
                return "EtG_OSX";
            }
            else if (osPlatform == OSPlatform.Linux)
            {
                switch (arch)
                {
                    case Architecture.X64:
                        return "EtG.x86_64";
                    case Architecture.X86:
                        return "EtG.x86";
                }
            }

            throw new NotSupportedException($"Platform {osPlatform} {arch} is not supported");
        }

        public static string GetProcessName(OSPlatform osPlatform, Architecture arch)
        {
            if (osPlatform == OSPlatform.Windows)
            {
                return "EtG";
            }
            else if (osPlatform == OSPlatform.OSX)
            {
                return "EtG_OSX";
            }
            if (osPlatform == OSPlatform.Linux)
            {
                switch (arch)
                {
                    case Architecture.X64:
                        return "EtG.x86_64";
                    case Architecture.X86:
                        return "EtG.x86";
                }
            }

            throw new NotSupportedException($"Platform {osPlatform} {arch} is not supported");
        }
    }
}
