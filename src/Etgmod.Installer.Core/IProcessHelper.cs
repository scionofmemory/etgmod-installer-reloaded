using System.Collections.Generic;

namespace Etgmod.Installer.Core
{
    public interface IProcessHelper
    {
        void CloseEtgProcesses();

        IEnumerable<(string processName, string mainModuleFile)> GetEtgProcesses();
    }
}
