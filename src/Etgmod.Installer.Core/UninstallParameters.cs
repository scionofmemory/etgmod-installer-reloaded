namespace Etgmod.Installer.Core
{
    public sealed class UninstallParameters
    {
        public bool ForceClose { get; set; }
    }
}
