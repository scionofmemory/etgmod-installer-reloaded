﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Etgmod.Installer.Core.Discovery
{
    public interface IDiscoverer
    {
        string Name { get; }

        IReadOnlyList<OSPlatform> SupportedPlatforms { get; }

        void Initialize();

        DiscoveryInformation DiscoverEtgInstallation();
    }
}
