﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;

namespace Etgmod.Installer.Core.Discovery
{
    public class SteamDiscoverer : IDiscoverer
    {
        private const string Source = "Steam";
        private const int SteamAppId = 311690;

        private readonly IFileSystem _fileSystem;
        private readonly EtgPlatformSpecifics _platformSpecifics;
        private readonly ILogger _logger;

        private readonly List<string> _steamAppsNames;

        private string _steamInstallationFolder;

        private List<(string, Func<string>)> _discoveryInitSteps;

        public SteamDiscoverer(IFileSystem fileSystem, EtgPlatformSpecifics platformSpecifics, ILogger<SteamDiscoverer> logger)
        {
            _fileSystem = fileSystem;
            _platformSpecifics = platformSpecifics;
            _logger = logger;
            _discoveryInitSteps = new List<(string, Func<string>)>()
            {
                ("Process", FindSteamByProcess),
                ("CommonPath", FindSteamByGuess)
            };

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                _discoveryInitSteps.Insert(0, ("Registry", FindSteamByRegistry));
            }

            _steamAppsNames = new List<string>()
            {
                "steamapps"
            };

            // seems like it could be platform dependent, this was in the old installer.
            // however I only saw the lower case, version when testing mac
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                _steamAppsNames.Add("SteamApps");
            }
        }

        public string Name => "Steam";

        public IReadOnlyList<OSPlatform> SupportedPlatforms { get; } = new[] { OSPlatform.Windows, OSPlatform.Linux, OSPlatform.OSX };

        public void Initialize()
        {
            foreach (var (name, step) in _discoveryInitSteps)
            {
                string path = step();
                if (!string.IsNullOrEmpty(path))
                {
                    _logger.LogInformation("Steam located by {Method}: {Path}", name, path);
                    _steamInstallationFolder = path;
                    return;
                }
            }
        }

        public DiscoveryInformation DiscoverEtgInstallation()
        {
            if (_steamInstallationFolder == null)
            {
                _logger.LogDebug("Could not locate steam");
                return DiscoveryInformation.MissingApplication;
            }

            string etg = CheckForAppInLibrary(_steamInstallationFolder);
            if (etg != null)
            {
                return CreateDiscoInfo(etg);
            }

            // checking other libraries
            string libraryFoldersVdf = CheckSteamNamesForFile(_steamInstallationFolder, "libraryfolders.vdf");
            if (libraryFoldersVdf == null)
            {
                return DiscoveryInformation.MissingApplication;
            }

            string[] vdfLines = _fileSystem.FileReadAllLines(libraryFoldersVdf);
            foreach (string line in vdfLines)
            {
                string[] parts = line.Split('\t', StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                {
                    continue;
                }

                if (!int.TryParse(parts[0].Trim('\"'), out _))
                {
                    continue;
                }

                string libraryPath = parts[1].Trim('\"').Replace("\\\\", "\\");
                if (!_fileSystem.DirectoryExists(libraryPath))
                {
                    _logger.LogDebug("Steam library directory was not found:{Path}", libraryPath);
                    continue;
                }

                etg = CheckForAppInLibrary(libraryPath);
                if (etg != null)
                {
                    return CreateDiscoInfo(etg);
                }
            }

            return DiscoveryInformation.MissingApplication;
        }

        private static DiscoveryInformation CreateDiscoInfo(string rawPath)
        {
            return new DiscoveryInformation(new ApplicationPath(rawPath), Source);
        }

        private string CheckForAppInLibrary(string libraryPath)
        {
            string acf = CheckSteamNamesForFile(libraryPath, $"appmanifest_{SteamAppId}.acf");
            if (acf == null)
            {
                return null;
            }

            string platformExecutable = _platformSpecifics.ExecutableName;
            string etg;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                // a typical OSX path is /Users/$USER/Library/Application Support/Steam/SteamApps/common/Enter the Gungeon/EtG_OSX.app/Contents/MacOS/EtG_OSX
                // LibraryPath = /Users/$USER/Library/Application Support/Steam
                string steamEtg = Path.Combine("common", "Enter the Gungeon", "EtG_OSX.app", "Contents", "MacOS", "EtG_OSX");
                etg = CheckSteamNamesForFile(libraryPath, steamEtg);
                if (etg != null)
                {
                    return etg;
                }
            }
            else
            {
                string steamEtg = Path.Combine("common", "Enter the Gungeon", platformExecutable);
                etg = CheckSteamNamesForFile(libraryPath, steamEtg);
                if (etg != null)
                {
                    return etg;
                }
            }

            return null;
        }

        private string FindSteamByProcess()
        {
            try
            {
                _logger.LogDebug("Checking for steam processes");
                var processes = Process.GetProcesses();
                foreach (var process in processes)
                {
                    if (!process.ProcessName.Contains("steam"))
                    {
                        continue;
                    }

                    if (process.MainModule.ModuleName.ToLower().Contains("steam"))
                    {
                        string path = Path.GetDirectoryName(process.MainModule.FileName);
                        if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                        {
                            path = Path.GetDirectoryName(path);
                        }
                        else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                        {
                            // we are probably in the steam app bundle at the directory
                            // $HOME/Library/Application Support/Steam/Steam.AppBundle/Steam/Contents/MacOS
                            // and we want to go to
                            // $HOME/Library/Application Support/Steam/steamapps
                            path = Path.GetDirectoryName(path) ?? string.Empty; // Contents
                            path = Path.GetDirectoryName(path) ?? string.Empty; // Steam
                            path = Path.GetDirectoryName(path) ?? string.Empty; // Steam.AppBundle
                            path = Path.GetDirectoryName(path) ?? string.Empty; // Steam
                        }

                        _logger.LogDebug("Steam found at {SteamPath}", path);
                        return path;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogDebug(e, "Failed to inspect steam processes");
            }

            return null;
        }

        private string FindSteamByRegistry()
        {
            using var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            using var steamKey = hklm.OpenSubKey(@"SOFTWARE\WOW6432Node\Valve\Steam");
            string path;
            if (steamKey != null)
            {
                path = steamKey.GetValue("InstallPath") as string;
                if (!string.IsNullOrEmpty(path) && _fileSystem.DirectoryExists(path))
                {
                    return path;
                }
            }

            return null;
        }

        private string FindSteamByGuess()       
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                string path = Path.Combine(Environment.GetEnvironmentVariable("HOME"), ".local/share/Steam");
                if (_fileSystem.DirectoryExists(path))
                {
                    return path;
                }
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                // $HOME/Library/Application Support/Steam/SteamApps/common/Enter the Gungeon/EtG_OSX.app/
                string path = Path.Combine(Environment.GetEnvironmentVariable("HOME"), "Library/Application Support/Steam");
                if (_fileSystem.DirectoryExists(path))
                {
                    return path;
                }
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Steam");
                if (_fileSystem.DirectoryExists(path))
                {
                    return path;
                }

                path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Steam");
                if (_fileSystem.DirectoryExists(path))
                {
                    return path;
                }
            }

            return null;
        }

        private string CheckSteamNamesForFile(string pathAboveSteamName, string pathBelowSteamNames)
        {
            string foundPath = null;
            foreach (string name in _steamAppsNames)
            {
                string candidatePath = Path.Combine(pathAboveSteamName, name, pathBelowSteamNames);
                if (_fileSystem.FileExists(candidatePath))
                {
                    foundPath = candidatePath;
                    break;
                }
            }

            return foundPath;
        }
    }
}
