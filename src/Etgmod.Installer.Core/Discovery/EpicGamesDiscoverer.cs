using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core.Discovery
{
    public class EpicGamesDiscoverer : IDiscoverer
    {
        public static readonly string SourceName = "Epic Games";

        // This is platform independent value.
        public static readonly string EnterTheGungeonCatalogItemId = "25b771db01554b1bb40c5617b663d17e";

        public static readonly string DefaultEpicGamesManifestPath = GetSystemDefaultEpicGamesManifestPath();

        private readonly IFileSystem _fileSystem;
        private readonly EtgPlatformSpecifics _platformSpecifics;
        private readonly ILogger _logger;

        private bool _enabled;

        public EpicGamesDiscoverer(
            IFileSystem fileSystem,
            EtgPlatformSpecifics platformSpecifics,
            ILogger<EpicGamesDiscoverer> logger)
        {
            _fileSystem = fileSystem;
            _platformSpecifics = platformSpecifics;
            _logger = logger;
        }

        public string EpicGamesManifestPath { get; set; } = DefaultEpicGamesManifestPath;

        public string Name => "Epic";

        public IReadOnlyList<OSPlatform> SupportedPlatforms { get; } = new[] { OSPlatform.Windows, OSPlatform.OSX };

        public void Initialize()
        {
            _enabled = _fileSystem.DirectoryExists(EpicGamesManifestPath);
        }

        public DiscoveryInformation DiscoverEtgInstallation()
        {
            if (!_enabled)
            {
                return DiscoveryInformation.MissingApplication;
            }

            foreach (string manifestFile in _fileSystem.EnumerateFiles(EpicGamesManifestPath, "*.item"))
            {
                try
                {
                    string fileText = _fileSystem.FileReadAllText(manifestFile);
                    var manifestData = JsonSerializer.Deserialize<EpicManifestData>(fileText);
                    if (manifestData?.CatalogItemId != EnterTheGungeonCatalogItemId)
                    {
                        continue;
                    }

                    if (manifestData?.InstallLocation == null)
                    {
                        _logger.LogDebug("Failed to read InstallLocation from manifest file: {Path}", manifestFile);
                        continue;
                    }

                    string gamePath = Path.Combine(manifestData.InstallLocation, manifestData.LaunchExecutable ?? string.Empty);
                    if (_fileSystem.FileExists(gamePath))
                    {
                        return new DiscoveryInformation(new ApplicationPath(gamePath), SourceName);
                    }
                    else
                    {
                        _logger.LogDebug("Path retrieved from manifest does not have game file {Path}", gamePath);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogDebug(e, "Failed to read manifest file {Path}", manifestFile);
                }
            }

            return DiscoveryInformation.MissingApplication;
        }

        private static string GetSystemDefaultEpicGamesManifestPath()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return Environment.ExpandEnvironmentVariables(@"%ProgramData%\Epic\EpicGamesLauncher\Data\Manifests");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return Environment.ExpandEnvironmentVariables(@"%HOME%/Library/Application Support/Epic/EpicGamesLauncher/Data/Manifests");
            }
            else
            {
                return string.Empty;
            }
        }

        private class EpicManifestData
        {
            public string LaunchExecutable { get; set; }

            public string InstallLocation { get; set; }

            public string CatalogItemId { get; set; }

            public string DisplayName { get; set; }
        }
    }
}
