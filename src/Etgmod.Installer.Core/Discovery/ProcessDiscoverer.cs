using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core.Discovery
{
    public class ProcessDiscoverer : IDiscoverer
    {
        private const string Source = "Process";

        private readonly IProcessHelper _processHelper;
        private readonly ILogger _logger;

        public ProcessDiscoverer(IProcessHelper processHelper, ILogger<ProcessDiscoverer> logger)
        {
            _processHelper = processHelper;
            _logger = logger;
        }

        public string Name => "Process";

        public IReadOnlyList<OSPlatform> SupportedPlatforms { get; } = new[] { OSPlatform.Windows, OSPlatform.Linux, OSPlatform.OSX };

        public void Initialize()
        {
            // intentionally nothing to do here.
        }

        public DiscoveryInformation DiscoverEtgInstallation()
        {
            var processes = _processHelper.GetEtgProcesses();
            foreach (var (processName, fileName) in processes)
            {
                _logger.LogDebug("EtG found at {ProcessFilePath}", fileName);
                return new DiscoveryInformation(new ApplicationPath(fileName), Source);
            }

            _logger.LogDebug("Did not locate EtG by process");
            return DiscoveryInformation.MissingApplication;
        }
    }
}
