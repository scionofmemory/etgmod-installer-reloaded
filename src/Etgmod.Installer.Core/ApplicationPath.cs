using System;

namespace Etgmod.Installer.Core
{
    /// EtG is an executable file on Windows and Linux but an app bundle on Mac.
    /// This class abstracts some of the differences to make handling paths simpler.
    public sealed class ApplicationPath
    {
        public ApplicationPath(string rawPath)
        {
            RawPath = rawPath ?? throw new ArgumentNullException(nameof(rawPath));
        }

        public string RawPath { get; }

        public override string ToString() => RawPath;
    }
}
