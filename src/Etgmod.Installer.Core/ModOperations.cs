﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Etgmod.Installer.Core
{
    public sealed class ModOperations : IModOperations
    {
        private readonly IFileSystem _fileSystem;
        private readonly IReporter _reporting;
        private readonly EtgPaths _paths;
        private readonly EtgPlatformSpecifics _platformSpecifics;
        private readonly EtgInstallationInfo _installationInfo;
        private readonly List<BackupItem> _backupSequence;
        private readonly ILogger _logger;

        public ModOperations(
            IFileSystem fileSystem,
            IReporter reporting,
            EtgPaths paths,
            EtgPlatformSpecifics platformSpecifics,
            EtgInstallationInfo installationInfo,
            ILogger<ModOperations> logger)
        {
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _reporting = reporting ?? throw new ArgumentNullException(nameof(reporting));
            _paths = paths ?? throw new ArgumentNullException(nameof(paths));
            _platformSpecifics = platformSpecifics ?? throw new ArgumentNullException(nameof(platformSpecifics));
            _installationInfo = installationInfo ?? throw new ArgumentNullException(nameof(installationInfo));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _backupSequence = new List<BackupItem>()
            {
                new BackupItem(ps => ps.InstalledUnityEngineCoreModuleFile),
                new BackupItem(ps => ps.InstalledAssemblyCsharpFile),
                new BackupItem(ps => ps.PatchTargetFile)
            };
        }

        public static readonly Version CurrentInstallerVersion = new Version(19, 4, 12);

        public void Backup()
        {
            string backupDirectoryPath = _paths.ModBackupDirectory;
            _fileSystem.EnsureDirectoryExists(backupDirectoryPath);

            foreach (var backupItem in _backupSequence)
            {
                string origPath = backupItem.GetSourceFilePath(_paths);
                string fileName = Path.GetFileName(origPath);
                if (!_fileSystem.FileExists(origPath))
                {
                    _logger.LogWarning("File to backup {Path} was missing", origPath);
                    continue;
                }

                _reporting.Log($"Backing up: {fileName}");
                _logger.LogInformation("Backing up {Path}", origPath);
                string backupFilePath = Path.Combine(backupDirectoryPath, fileName);
                _fileSystem.FileCopy(origPath, backupFilePath, true);
            }
        }

        public void ClearBackup()
        {
            string pathBackup = _paths.ModBackupDirectory;
            if (!_fileSystem.DirectoryExists(pathBackup))
            {
                return;
            }

            _reporting.Log("Clearing mod backup...");

            string[] files = _fileSystem.GetFiles(pathBackup);
            _reporting.InitProgress("Clearing mod backup", files.Length + 1);
            for (int i = 0; i < files.Length; i++)
            {
                string file = Path.GetFileName(files[i]);
                _reporting.Log($"Removing: {file}");
                _reporting.SetProgress("Removing: " + file, i);
                _fileSystem.FileDelete(files[i]);
            }

            _reporting.EndProgress("Clearing backup complete.");
        }

        public void ClearCache()
        {
            string pathCache = _paths.ModCacheDirectory;
            if (!_fileSystem.DirectoryExists(pathCache))
            {
                return;
            }

            _reporting.Log("Clearing mod cache...");

            string[] files = _fileSystem.GetFiles(pathCache);
            _reporting.InitProgress("Clearing mod cache", files.Length + 1);
            for (int i = 0; i < files.Length; i++)
            {
                string file = Path.GetFileName(files[i]);
                _reporting.Log($"Removing: {file}");
                _reporting.SetProgress("Removing: " + file, i);
                _fileSystem.FileDelete(files[i]);
            }

            _reporting.EndProgress("Clearing cache complete.");
        }

        public void RestoreFromBackup()
        {
            _logger.LogInformation("Starting revert backup in {Path}", _paths.GameDirectory);
            string pathBackup = _paths.ModBackupDirectory;
            if (!_fileSystem.DirectoryExists(pathBackup))
            {
                _logger.LogInformation("Backup directory not found {Path}", pathBackup);
                return;
            }

            _logger.LogDebug("Backup path: {Path}", pathBackup);

            string[] files = _fileSystem.GetFiles(_paths.ManagedDirectory);
            _reporting.InitProgress("Removing leftover files", files.Length + 1);
            for (int i = 0; i < files.Length; i++)
            {
                string file = Path.GetFileName(files[i]);
                if (!file.Contains(".mm."))
                {
                    continue;
                }
                
                _reporting.Log($"Removing: {file}");
                _logger.LogInformation("Removing {Path}", files[i]);
                _reporting.SetProgress($"Removing: {file}", i);
                _fileSystem.FileDelete(files[i]);
            }

            string assemblyCsharp = _paths.InstalledAssemblyCsharpFile;
            string modVersion = GetModVersion(assemblyCsharp);
            if (modVersion != null)
            {
                _reporting.Log($"Found previous mod installation: {modVersion}");
                _reporting.Log("Reverting to unmodded backup...");
            }
            else
            {
                _reporting.Log("No previous mod installation found.");
                _reporting.Log("Still reverting to unmodded backup...");
            }

            int skipped = 0;
            _reporting.InitProgress("Uninstalling ETGMod", _backupSequence.Count + 1);
            int backupCounter = 0;
            foreach (var backupItem in _backupSequence)
            {
                string restoreTargetPath = backupItem.GetSourceFilePath(_paths);
                string targetFileName = Path.GetFileName(restoreTargetPath);
                string backupFilePath = Path.Combine(_paths.ModBackupDirectory, targetFileName);

                if (!_fileSystem.FileExists(backupFilePath))
                {
                    _logger.LogInformation("Skipped restore missing file {BackupFilePath} -> {TargetFilePath}", backupFilePath, targetFileName);
                    _reporting.Log($"WARNING: Restoring back up of {targetFileName} failed because the backup was not found");
                    skipped++;
                    continue;
                }

                _reporting.Log($"Reverting: {targetFileName}");
                _reporting.SetProgress($"Reverting: {targetFileName}", backupCounter);

                _fileSystem.EnsureFileDeleted(restoreTargetPath);
                _fileSystem.FileMove(backupFilePath, restoreTargetPath);
                _logger.LogInformation("Successfully restored {BackupFilePath} -> {TargetFilePath}", backupFilePath, restoreTargetPath);
                backupCounter++;
            }

            if (skipped > 0)
            {
                _logger.LogWarning("Uninstall finished but some files could not be restored");
            }

            _reporting.EndProgress("Revert backup complete.");
        }

        public bool UnzipMod(Stream zipStream)
        {
            string platform = "";
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                platform = "win32";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                platform = "osx";
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                platform = Environment.Is64BitOperatingSystem ? "lib64" : "lib";
            }

            string fallback = "ETGMOD";
            string prefix = fallback;
            string installedVersion = _installationInfo.GetInstalledGameVersion();
            if (installedVersion == null)
            {
                _logger.LogWarning("Did not get installed EtG version!");
                throw new InstallationFailedException("Could not find installed version");
            }

            if (installedVersion.Contains("b"))
            {
                prefix += "-BETA";
            }

            fallback += "/";
            prefix += "/";

            var stopwatch = Stopwatch.StartNew();
            _reporting.Log($"Checking for {prefix}...");

            using var zip = new ZipArchive(zipStream, ZipArchiveMode.Read);

            int prefixCount = 0;
            int fallbackCount = 0;
            int noneCount = 0;
            _reporting.InitProgress("Scanning ZIP", zip.Entries.Count);
            for (int i = 0; i < zip.Entries.Count; i++)
            {
                _reporting.SetProgress(i);
                ZipArchiveEntry entry = zip.Entries[i];
                _reporting.Log($"Entry: {entry.FullName}: {entry.Length} bytes");

                if (entry.FullName == "InstallerVersion.txt")
                {
                    _reporting.Log("Found version file.");

                    using var stream = entry.Open();
                    using (var sr = new StreamReader(stream))
                    {
                        Version minv = new Version(sr.ReadLine().Trim());
                        if (CurrentInstallerVersion < minv)
                        {
                            _reporting.Log("There's a new ETGMod Installer version!");
                            _reporting.Log($"Visit https://modthegungeon.github.io/#download to download it. (Minimum installer version for this ETGMod version: {minv})");
                            _reporting.ImportantProgressMessage("Installer update required!");
                            return false;
                        }
                    }

                    continue;
                }

                string entryName = entry.FullName;
                if (entry.FullName.StartsWith(prefix))
                {
                    prefixCount++;
                }
                else if (entry.FullName.StartsWith(fallback))
                {
                    fallbackCount++;
                }
                else
                {
                    noneCount++;
                }
            }

            if (0 < prefixCount)
            {
                _reporting.Log($"{prefix} found.");
                _reporting.InitProgress("Extracting ZIP", prefixCount);
            }
            else if (0 < fallbackCount)
            {
                _reporting.Log($"Didn't find {prefix} falling back to {fallback}.");
                prefix = fallback;
                _reporting.InitProgress("Extracting ZIP", fallbackCount);
            }
            else
            {
                _reporting.Log("Is this even a ETGMod ZIP? uh...");
                prefix = "";
                _reporting.InitProgress("Extracting ZIP", noneCount);
            }

            int extracted = 0;
            foreach (var entry in zip.Entries)
            {
                if (!entry.FullName.StartsWith(prefix) || entry.FullName == prefix)
                {
                    continue;
                }

                _reporting.SetProgress(++extracted);

                string entryName = entry.FullName.Substring(prefix.Length);

                if (entryName.StartsWith("LIBS/"))
                {
                    entryName = entryName.Substring(5);
                    if (!entryName.StartsWith(platform + "/"))
                    {
                        continue;
                    }
                    entryName = entryName.Substring(platform.Length + 1);
                }

                entryName = entryName.Replace('/', Path.DirectorySeparatorChar);

                string path = Path.Combine(_paths.ManagedDirectory, entryName);
                _reporting.Log($"Extracting: {entry.FullName} -> {path}");
                if (entry.Length == 0 && entry.CompressedLength == 0)
                {
                    _fileSystem.CreateDirectory(path);
                }
                else
                {
                    entry.ExtractToFile(path, true);
                }
            }

            _reporting.EndProgress("Extracted ZIP");
            stopwatch.Stop();

            _logger.LogInformation("Extracted ZIP Elapsed:{Elapsed}", stopwatch.Elapsed);

            return true;
        }

        public string GetModVersion(string assemblyCsharpPath)
        {
            using var modder = new LoggingMonoModder(_reporting, _logger)
            {
                InputPath = assemblyCsharpPath
            };

            try
            {
                modder.Read();
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "Monomod could not read file: {Path}", assemblyCsharpPath);
                return null;
            }

            TypeDefinition modType = modder.Module.GetType("ETGMod");
            if (modType == null)
            {
                return null;
            }

            MethodDefinition modCctor = null;
            for (int i = 0; i < modType.Methods.Count; i++)
            {
                if (modType.Methods[i].IsStatic && modType.Methods[i].IsConstructor)
                {
                    modCctor = modType.Methods[i];
                    break;
                }
            }

            if (modCctor == null)
            {
                return null;
            }

            string modVersion = "";
            string modBuild = "";
            string modProfile = "";
            for (int i = 0; i < modCctor.Body.Instructions.Count; i++)
            {
                Instruction instructionField = modCctor.Body.Instructions[i];
                if (instructionField.OpCode != OpCodes.Stsfld)
                {
                    continue;
                }
                FieldReference field = (FieldReference)instructionField.Operand;
                if (field.Name == "BaseVersion")
                {
                    int count = ((MethodReference)modCctor.Body.Instructions[i - 1].Operand).Parameters.Count;
                    for (int ii = i - count - 1; ii < i - 1; ii++)
                    {
                        modVersion += modCctor.Body.Instructions[ii].GetInt();
                        if (ii < i - 2)
                        {
                            modVersion += ".";
                        }
                    }
                }
                if (field.Name == "BaseTravisBuild")
                {
                    int build = modCctor.Body.Instructions[i - 1].GetInt();
                    if (build != 0)
                    {
                        modBuild = "-" + build;
                    }
                }
                if (field.Name == "BaseProfile")
                {
                    string profile = modCctor.Body.Instructions[i - 2].Operand as string;
                    if (!string.IsNullOrEmpty(profile))
                    {
                        modProfile = "-" + profile;
                    }
                }
            }

            string version = modVersion + modBuild + modProfile;
            return string.IsNullOrEmpty(version) ? null : version;
        }

        private class BackupItem
        {
            private Func<EtgPaths, string> _sourceFileGetter;

            public BackupItem(Func<EtgPaths, string> sourceFileGetter)
            {
                _sourceFileGetter = sourceFileGetter ?? throw new ArgumentNullException(nameof(sourceFileGetter));
            }

            public string GetSourceFilePath(EtgPaths settings) => _sourceFileGetter(settings);
        }
    }
}
