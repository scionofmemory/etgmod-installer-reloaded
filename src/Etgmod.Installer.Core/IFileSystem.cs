﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Etgmod.Installer.Core
{
    public interface IFileSystem
    {
        bool DirectoryExists(string path);

        void CreateDirectory(string path);

        string[] GetFiles(string path);

        IEnumerable<string> EnumerateFiles(string path, string searchPath);

        bool FileExists(string path);

        bool EnsureFileDeleted(string path);

        void FileCopy(string sourceFileName, string destFileName, bool overwrite);

        void FileMove(string sourceFilePath, string destFilePath);

        void FileDelete(string path);

        string FileReadAllText(string path);

        Task<string> FileReadAllTextAsync(string path);

        string[] FileReadAllLines(string path);

        byte[] FileReadAllBytes(string path);

        void FileWriteAllBytes(string path, byte[] data);

        void FileWriteAllText(string path, string contents);

        Task FileWriteAllTextAsync(string path, string contents);

        Stream OpenFileStream(string path, FileMode mode);
    }

    public static class FileSystemExtensions
    {
        public static void EnsureDirectoryExists(this IFileSystem fs, string path)
        {
            if (!fs.DirectoryExists(path))
            {
                fs.CreateDirectory(path);
            }
        }
    }
}
