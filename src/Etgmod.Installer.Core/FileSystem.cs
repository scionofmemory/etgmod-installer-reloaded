﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.Core
{
    public sealed class FileSystem : IFileSystem
    {
        private readonly ILogger _logger;

        public FileSystem(ILogger<FileSystem> logger)
        {
            _logger = logger;
        }

        public void CreateDirectory(string path)
        {
            _logger.LogDebug("Create directory '{Path}'", path);
            Directory.CreateDirectory(path);
        }

        public bool DirectoryExists(string path)
        {
            _logger.LogDebug("Directory exists check '{Path}'", path);
            bool result = Directory.Exists(path);
            _logger.LogDebug("Directory exists result '{Path}': {Result}", path, result);
            return result;
        }

        public string[] GetFiles(string path)
        {
            _logger.LogDebug("Directory GetFiles:{Path}", path);
            return Directory.GetFiles(path);
        }

        public IEnumerable<string> EnumerateFiles(string path, string searchPattern)
        {
            _logger.LogDebug("Directory EnumerateFiles:{Path} {SearchPattern}", path, searchPattern);
            return Directory.EnumerateFiles(path, searchPattern);
        }

        public bool EnsureFileDeleted(string path)
        {
            _logger.LogDebug("Ensure file deleted exists check '{Path}'", path);
            if (File.Exists(path))
            {
                _logger.LogDebug("File Delete:{Path}", path);
                File.Delete(path);
                return true;
            }

            _logger.LogDebug("File does not exist '{Path}'", path);
            return false;
        }

        public void FileCopy(string sourceFileName, string destFileName, bool overwrite)
        {
            _logger.LogDebug("File Copy source:{Source}, dest:{Dest}, Overwrite:{Overwrite}", sourceFileName, destFileName, overwrite);
            File.Copy(sourceFileName, destFileName, overwrite);
        }

        public void FileDelete(string path)
        {
            _logger.LogDebug("File Delete:{Path}", path);
            File.Delete(path);
        }

        public bool FileExists(string path)
        {
            _logger.LogDebug("File exists check '{Path}'", path);
            bool result = File.Exists(path);
            _logger.LogDebug("File exists result '{Path}': {Result}", path, result);
            return result;
        }

        public void FileMove(string sourceFilePath, string destFilePath)
        {
            _logger.LogDebug("File Move, Source:{Source}, Dest:{Dest}", sourceFilePath, destFilePath);
            File.Move(sourceFilePath, destFilePath);
        }

        public string FileReadAllText(string path)
        {
            _logger.LogDebug("File FileReadAllText:{Path}", path);
            return File.ReadAllText(path);
        }

        public string[] FileReadAllLines(string path)
        {
            _logger.LogDebug("File FileReadAllLines:{Path}", path);
            return File.ReadAllLines(path);
        }

        public byte[] FileReadAllBytes(string path)
        {
            _logger.LogDebug("File ReadAllBytes:{Path}", path);
            return File.ReadAllBytes(path);
        }

        public void FileWriteAllBytes(string path, byte[] data)
        {
            _logger.LogDebug("File WriteAllBytes:{Path} {Length} bytes", path, data?.Length);
            File.WriteAllBytes(path, data);
        }

        public void FileWriteAllText(string path, string contents)
        {
            _logger.LogDebug("File WriteAllText:{Path} {Length} characters", path, contents?.Length);
            File.WriteAllText(path, contents);
        }

        public Stream OpenFileStream(string path, FileMode mode)
        {
            _logger.LogDebug("OpenFileStream:{Path}, Mode:{mode}", path, mode);
            return new FileStream(path, mode, FileAccess.ReadWrite);
        }

        public async Task<string> FileReadAllTextAsync(string path)
        {
            _logger.LogDebug("File FileReadAllTextAsync:{Path}", path);
            return await File.ReadAllTextAsync(path);
        }

        public async Task FileWriteAllTextAsync(string path, string contents)
        {
            _logger.LogDebug("File WriteAllTextAsync:{Path} {Length} characters", path, contents?.Length);
            await File.WriteAllTextAsync(path, contents);
        }
    }
}
