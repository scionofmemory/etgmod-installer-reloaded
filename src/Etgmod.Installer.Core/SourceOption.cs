namespace Etgmod.Installer.Core
{
    public enum SourceOption
    {
        AutomaticDownload,
        Offline,
        Zip
    }
}
