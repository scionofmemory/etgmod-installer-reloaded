﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Etgmod.Installer.Core;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer
{
    internal sealed class ConsoleHost
    {
        private readonly IFileSystem _fileSystem;
        private readonly IDiscoveryService _discoService;
        private readonly EtgModder _modder;
        private readonly EtgPaths _paths;
        private readonly ILogger _logger;

        public ConsoleHost(
            IFileSystem fileSystem,
            IDiscoveryService discoService,
            EtgModder modder,
            EtgPaths paths,
            ILogger<ConsoleHost> logger)
        {
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _discoService = discoService ?? throw new ArgumentNullException(nameof(discoService));
            _modder = modder ?? throw new ArgumentNullException(nameof(modder));
            _paths = paths ?? throw new ArgumentNullException(nameof(paths));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public TextWriter Out { get; set; } = Console.Out;

        public TextWriter Error { get; set; } = Console.Error;

        public bool Initialize(string userSpecifiedPath)
        {
            _logger.LogInformation("Command line: {Commandline}", Environment.CommandLine);
            _logger.LogInformation("CLI Version: {Version}", ThisAssembly.AssemblyInformationalVersion);
            _logger.LogInformation("Operating System: {OS} {Architecture}", RuntimeInformation.OSDescription, RuntimeInformation.OSArchitecture);
            _logger.LogInformation("Framework: {Framework}", RuntimeInformation.FrameworkDescription);

            try
            {
                _discoService.Initialize();
                ApplicationPath appPath;
                if (userSpecifiedPath == null)
                {
                    var discoInfo = _discoService.DiscoverEtgInstallation();
                    if (discoInfo.Found)
                    {
                        _logger.LogInformation("Discovered Etg path:{Path}", discoInfo.Path);
                        appPath = discoInfo.Path;
                    }
                    else
                    {
                        _logger.LogInformation("Could not locate Etg automatically");
                        appPath = null;
                    }
                }
                else
                {
                    appPath = new ApplicationPath(userSpecifiedPath);
                }

                if (appPath == null)
                {
                    Out.WriteLine("Enter the Gungeon not found!");
                    Out.WriteLine("Try using the --app option to specify the path");
                    return false;
                }

                _paths.SetApplicationPath(appPath);
                bool result = _paths.Exists(_fileSystem);
                if (!result)
                {
                    _logger.LogInformation("Could not locate {Path}", appPath);
                    Out.WriteLine("Enter the Gungeon not found!");
                    Out.WriteLine("Try using the --app option to specify the path");
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Initialization failed");
                return false;
            }
        }

        public async Task ExecuteInstall(InstallParameters parameters)
        {
            try
            {
                _logger.LogInformation($"CLI install started");
                await _modder.Install(parameters);
                _logger.LogInformation($"CLI install completed successfully");
            }
            catch (Exception e)
            {
                Error.WriteLine($"install failed: {e.Message}");
            }
            finally
            {
                _logger.LogInformation("CLI install ended");
            }
        }

        public async Task ExecuteUninstall(UninstallParameters parameters)
        {
            try
            {
                _logger.LogInformation($"CLI uninstall started");
                await _modder.Uninstall(parameters);
                _logger.LogInformation($"CLI uninstall completed successfully");
            }
            catch (Exception e)
            {
                Error.WriteLine($"uninstall failed: {e.Message}");
            }
            finally
            {
                _logger.LogInformation("CLI uninstall ended");
            }
        }

        public async Task ExecuteGetInfo()
        {
            try
            {
                _logger.LogInformation($"CLI info started");
                var info = await _modder.GetInfo();

                if (info.Found)
                {
                    Out.WriteLine($"Game directory: {info.GameDirectory}");
                    Out.WriteLine($"Installed game version: {info.InstalledGameVersion}");
                    Out.WriteLine($"Mod the Gungeon: {(info.IsModded ? "installed" : "not installed")}");
                    if (info.IsModded)
                    {
                        Out.WriteLine($"MtG version: {info.InstalledModVersion}");
                        Out.WriteLine($"Cached revision: {info.CacheRevision?.ToString() ?? "unknown"}");
                        Out.WriteLine($"Cached hash: {info.CachedSha2Hash ?? "(not found)"}");
                    }
                }
                else
                {
                    Out.WriteLine("Some Enter the Gungeon files were not found!");
                    Out.WriteLine($"Search location: {info.GameDirectory}");
                }
            }
            catch (Exception e)
            {
                Error.WriteLine($"info failed: {e.Message}");
            }
            finally
            {
                _logger.LogInformation("CLI info ended");
            }
        }
    }
}
