﻿using System;
using System.IO;
using System.Threading.Tasks;
using Etgmod.Installer.Core;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Etgmod.Installer
{
    class Program
    {
        private const string LogFileName = "etgmod_installer.log";

        private const string LogOutputTemplate = "{Timestamp:HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message}{NewLine}{Exception}";

        static async Task<int> Main(string[] args)
        {
            try
            {
                var app = new CommandLineApplication();
                app.Name = "Etgmod.Installer";
                app.HelpOption(inherited: true);
                app.UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.CollectAndContinue;

                app.Command("install", c =>
                {
                    c.Description = "Install Mod the Gungeon.";
                    var appPathOption = c.Option("-a|--app <APP>", "No automatic location of EtG. Explicitly specify the path to the Enter the Gungeon executable/application bundle.", CommandOptionType.SingleValue);
                    var logOption = c.Option("-l|--log", "Output to console as a log.", CommandOptionType.NoValue);
                    var verboseOption = c.Option("-v|--verbose", "Enables verbose logging.", CommandOptionType.NoValue);
                    var autoRunOption = c.Option("--autorun", "Automatically run EtG after install finishes.", CommandOptionType.NoValue);
                    var forceCloseOption = c.Option("--force-close", "Force EtG to close before performing any changes.", CommandOptionType.NoValue);
                    var offlineOption = c.Option("--offline", "Skips download steps. Requires ETGMod.zip is present in cache directory. (Exclusive with --zip)", CommandOptionType.NoValue);
                    var zipPathOption = c.Option("--zip <FILE>", "Specify path to ETGMOD.zip file. Skips any download. (Exclusive with --offline)", CommandOptionType.SingleValue);

                    c.OnExecuteAsync(async ct =>
                    {
                        var outputStyle = logOption.HasValue() ? ConsoleOutputStyle.Log : ConsoleOutputStyle.Basic;
                        SetupLogger(outputStyle, verboseOption.HasValue());
                        var sourceOption = SourceOption.AutomaticDownload;
                        if (zipPathOption.HasValue())
                        {
                            sourceOption = SourceOption.Zip;
                        }
                        else if (offlineOption.HasValue())
                        {
                            sourceOption = SourceOption.Offline;
                        }

                        var installParams = new InstallParameters()
                        {
                            AutoRun = autoRunOption.HasValue(),
                            ForceClose = forceCloseOption.HasValue(),
                            Source = sourceOption,
                            PackageZipFilePath = zipPathOption.Value()
                        };

                        var services = CreateServiceCollection(outputStyle);
                        using var serviceProvider = services.BuildServiceProvider();
                        var host = serviceProvider.GetRequiredService<ConsoleHost>();
                        if (!host.Initialize(appPathOption.Value()))
                        {
                            return;
                        }

                        await host.ExecuteInstall(installParams);
                    });
                });

                app.Command("uninstall", c =>
                {
                    c.Description = "Remove Mod the Gungeon from the current installation.";
                    var appPathOption = c.Option("-a|--app <APP>", "No automatic location of EtG. Specifies the path to the Enter the Gungeon executable/application bundle.", CommandOptionType.SingleValue);
                    var logOption = c.Option("-l|--log", "Output to console as a log.", CommandOptionType.NoValue);
                    var verboseOption = c.Option("-v|--verbose", "Enables verbose logging.", CommandOptionType.NoValue);
                    var forceCloseOption = c.Option("--force-close", "Force EtG to close before performing any changes.", CommandOptionType.NoValue);

                    c.OnExecuteAsync(async ct =>
                    {
                        var outputStyle = logOption.HasValue() ? ConsoleOutputStyle.Log : ConsoleOutputStyle.Basic;
                        SetupLogger(outputStyle, verboseOption.HasValue());

                        var uninstallParameters = new UninstallParameters()
                        {
                            ForceClose = forceCloseOption.HasValue()
                        };

                        var services = CreateServiceCollection(outputStyle);
                        using var serviceProvider = services.BuildServiceProvider();
                        var host = serviceProvider.GetRequiredService<ConsoleHost>();
                        if (!host.Initialize(appPathOption.Value()))
                        {
                            return;
                        }

                        await host.ExecuteUninstall(uninstallParameters);
                    });
                });

                app.Command("info", c =>
                {
                    c.Description = "Display info on the current installed.";
                    var appPathOption = c.Option("-a|--app <APP>", "Explicitly specify the path to the Enter the Gungeon executable/application bundle.", CommandOptionType.SingleValue);
                    var logOption = c.Option("-l|--log", "Output to console as a log.", CommandOptionType.NoValue);
                    var verboseOption = c.Option("-v|--verbose", "Enables verbose logging.", CommandOptionType.NoValue);

                    c.OnExecuteAsync(async ct =>
                    {
                        var outputStyle = logOption.HasValue() ? ConsoleOutputStyle.Log : ConsoleOutputStyle.Basic;
                        SetupLogger(outputStyle, verboseOption.HasValue());

                        var services = CreateServiceCollection(outputStyle);

                        using var serviceProvider = services.BuildServiceProvider();
                        var host = serviceProvider.GetRequiredService<ConsoleHost>();
                        if (!host.Initialize(appPathOption.Value()))
                        {
                            return;
                        }

                        await host.ExecuteGetInfo();
                    });
                });

                app.OnExecute(() =>
                {
                    if (app.RemainingArguments.Count > 0)
                    {
                        Console.Error.WriteLine($"Unrecognized argument: {app.RemainingArguments[0]}");
                    }
                    else
                    {
                        Console.Error.WriteLine("Specify a subcommand");
                    }
                    app.ShowHelp();
                    return 1;
                });

                return await app.ExecuteAsync(args);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"There was an error: {e}");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IServiceCollection CreateServiceCollection(ConsoleOutputStyle outputStyle)
        {
            var services = new ServiceCollection()
                .AddLogging(lb => lb.AddSerilog(dispose: true))
                .AddEtgmodInstallerServices()
                .AddTransient<ConsoleHost>();

            if (outputStyle == ConsoleOutputStyle.Basic)
            {
                services
                    .AddSingleton<Core.IReporter, ConsoleReporter>();
            }
            else
            {
                services
                    .AddLoggingReporter();
            }

            return services;
        }

        private static void SetupLogger(ConsoleOutputStyle outputStyle, bool isVerbose)
        {
            var config = new LoggerConfiguration()
                .Enrich.FromLogContext();

            if (outputStyle == ConsoleOutputStyle.Log)
            {
                config.WriteTo.Console(outputTemplate: LogOutputTemplate);
            }

            if (isVerbose)
            {
                config.MinimumLevel.Debug();
            }
            else
            {
                config.MinimumLevel.Information();
            }

            string logFilePath = Path.Combine(AppContext.BaseDirectory, LogFileName);
            if (!string.IsNullOrEmpty(logFilePath))
            {
                if (File.Exists(logFilePath))
                {
                    File.Delete(logFilePath);
                }

                config.WriteTo.File(logFilePath, outputTemplate: LogOutputTemplate, fileSizeLimitBytes: 10_000_000);
            }

            Log.Logger = config.CreateLogger();
        }

        private enum ConsoleOutputStyle
        {
            Log,
            Basic
        }
    }
}
