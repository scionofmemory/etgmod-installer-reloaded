using System;
using System.IO;
using Etgmod.Installer.Core;

namespace Etgmod.Installer
{
    public class ConsoleReporter : IReporter
    {
        public TextWriter Out { get; set; } = Console.Out;

        public void ImportantProgressMessage(string message)
        {
        }

        public void InitProgress(string message, int max)
        {
            Out.WriteLine($"Starting {message}");
        }

        public void EndProgress(string message)
        {
            Out.WriteLine($"Completed {message}");
        }

        public void Log(string message)
        {
            Out.WriteLine(message);
        }

        public void OnActivity()
        {
        }

        public void SetProgress(int value)
        {
        }

        public void SetProgress(string message, int value)
        {
        }
    }
}