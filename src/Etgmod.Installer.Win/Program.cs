﻿using System;
using Etgmod.Installer.UI;

namespace Etgmod.Installer.Win
{
    class MainClass
    {
        [STAThread]
        public static void Main(string[] args)
        {
            new EtgModInstallerApp(Eto.Platforms.WinForms).Run();
        }
    }
}
