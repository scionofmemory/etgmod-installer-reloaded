﻿using System;
using System.IO;
using Etgmod.Installer.UI;
using Serilog;

namespace Etgmod.Installer.Gtk
{
    class MainClass
    {
        [STAThread]
        public static void Main(string[] args)
        {
            new EtgModInstallerApp(Eto.Platforms.Gtk).Run();
        }
    }
}
