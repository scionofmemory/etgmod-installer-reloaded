﻿using Eto.Drawing;
using Eto.Forms;

namespace Etgmod.Installer.UI
{
    public partial class FilePathControl : Panel
    {
        public FilePathControl()
        {
            FilePathTextBox = new TextBox();
            OpenFileButton = new Button()
            {
                Image = Icon.FromResource("Etgmod.Installer.UI.Assets.icons.open.png"),
                ToolTip = "Open"
            };

            Content = new TableLayout()
            {
                Spacing = new Size(3, 3),
                Rows =
                {
                    new TableRow(new TableCell(FilePathTextBox, true), new TableCell(OpenFileButton))
                }
            };
        }

        public TextBox FilePathTextBox { get; private set; }

        public Button OpenFileButton { get; private set; }
    }
}
