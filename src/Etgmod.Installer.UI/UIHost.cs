using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Etgmod.Installer.Core;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.UI
{
    public sealed class UIHost
    {
        private readonly IFileSystem _fileSystem;
        private readonly IDiscoveryService _discoService;
        private readonly EtgModder _modder;
        private readonly EtgPaths _paths;
        private readonly ILogger _logger;

        public UIHost(
            IFileSystem fileSystem,
            IDiscoveryService discoService,
            EtgModder modder,
            EtgPaths paths,
            ILogger<UIHost> logger)
        {
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _discoService = discoService ?? throw new ArgumentNullException(nameof(discoService));
            _modder = modder ?? throw new ArgumentNullException(nameof(modder));
            _paths = paths ?? throw new ArgumentNullException(nameof(paths));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            Status = HostStatus.None;
        }

        public ApplicationPath Path { get; private set; }

        public HostStatus Status { get; private set; }

        public void Initialize()
        {
            _logger.LogInformation("Installer Version: {Version}", ThisAssembly.AssemblyInformationalVersion);
            _logger.LogInformation("Operating System: {OS} {Architecture}", RuntimeInformation.OSDescription, RuntimeInformation.OSArchitecture);
            _logger.LogInformation("Framework: {Framework}", RuntimeInformation.FrameworkDescription);
            _discoService.Initialize();
        }

        public bool DiscoverApplication()
        {
            var discoInfo = _discoService.DiscoverEtgInstallation();
            if (discoInfo.Found)
            {
                Path = discoInfo.Path;
                _paths.SetApplicationPath(Path);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetApplicationPathFromInput(string userInput)
        {
            _logger.LogInformation("Setting application path: {InputPath}", userInput);
            Path = new ApplicationPath(userInput);
            _paths.SetApplicationPath(Path);
            bool result = _paths.Exists(_fileSystem);
            if (!result)
            {
                _logger.LogInformation("Failed to verify application paths: {InputPath}", userInput);
            }

            return result;
        }

        public async Task Install(InstallParameters parameters)
        {
            try
            {
                _logger.LogInformation("UI install started");
                await _modder.Install(parameters);
                Status = HostStatus.Success;
            }
            catch (Exception)
            {
                Status = HostStatus.Failure;
            }
            finally
            {
                _logger.LogInformation("UI install finished: {Status}", Status);
            }
        }

        public async Task Uninstall(UninstallParameters parameters)
        {
            try
            {
                _logger.LogInformation("UI uninstall started");
                await _modder.Uninstall(parameters);
                Status = HostStatus.Success;
            }
            catch (Exception)
            {
                Status = HostStatus.Failure;
            }
            finally
            {
                _logger.LogInformation("UI uninstall finished: {Status}", Status);
            }
        }

        public async Task<GameInstallationInfo> GetInfo()
        {
            var installInfo = new GameInstallationInfo() { Found = false };
            try
            {
                _logger.LogInformation($"UI info started");
                installInfo = await _modder.GetInfo();
            }
            catch (Exception)
            {
                installInfo = new GameInstallationInfo() { Found = false };
            }
            finally
            {
                _logger.LogInformation("UI info finished: {Found}", installInfo.Found);
            }

            return installInfo;
        }
    }
}
