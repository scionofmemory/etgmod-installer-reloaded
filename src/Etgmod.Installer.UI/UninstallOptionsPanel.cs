using System;
using Eto.Drawing;
using Eto.Forms;

namespace Etgmod.Installer.UI
{
    public partial class UninstallOptionsPanel : Panel
    {
        public UninstallOptionsPanel()
        {
            ForceCloseCheckBox = new CheckBox()
            {
                Text = "Force Close",
                ToolTip = "Force any running Enter the Gungeon processes to close before running the operation."
            };

            UninstallButton = new Button()
            {
                Text = "Uninstall",
                ToolTip = "Uninstall Mod the Gungeon"
            };

            Content = new TableLayout()
            {
                Padding = new Padding(5),
                Spacing = new Size(3, 3),
                Rows = 
                {
                    new TableRow(new TableCell(ForceCloseCheckBox)),
                    new TableRow() { ScaleHeight = true },
                    new TableRow(new TableCell(null, true), new TableCell(UninstallButton)),
                }
            };
        }

        public CheckBox ForceCloseCheckBox { get; set; }

        public Button UninstallButton { get; set; }
    }
}
