using System;
using Eto.Forms;
using Eto.Drawing;
using Etgmod.Installer.Core;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Diagnostics;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Logging;

namespace Etgmod.Installer.UI
{
    public partial class InstallerWindow : Form
    {
        public InstallerWindow()
        {
            Title = "ETGMod Installer: Reloaded";
            Icon = Icon.FromResource("Etgmod.Installer.UI.Assets.icons.app.ico");
            MinimumSize = new Size(700, 600);

            InfoLabel = new Label();
            StatusImageView = new ImageView()
            {
                Size = new Size(216 / 4, 152 / 4)
            };

            LogTextArea = new RichTextArea()
            {
                ReadOnly = true
            };

            ExePathControl = new FilePathControl();
            ExePathControl.FilePathTextBox.LostFocus += ExePathChanged;
            ExePathControl.OpenFileButton.Click += OpenFileClicked;

            OpenDetailLogButton = new Button()
            {
                Text = "Details...",
                ToolTip = "Open detailed log"
            };

            OpenDetailLogButton.Click += OpenDetailLogClicked;

            InstallPanel = new InstallOptionsPanel();
            InstallPanel.InstallButton.Click += InstallClicked;

            UninstallPanel = new UninstallOptionsPanel();
            UninstallPanel.UninstallButton.Click += UninstallClicked;

            TabControl = new TabControl();
            var sc = new Scrollable() { Content = InstallPanel };
            TabControl.Pages.Add(new TabPage(sc)
            {
                Text = "Install"
            });

            TabControl.Pages.Add(new TabPage(UninstallPanel)
            {
                Text = "Uninstall"
            });
            TabControl.Size = new Size(-1, 170);

            var infoTable = new TableLayout()
            {
                Rows =
                {
                    new TableRow(new TableCell(InfoLabel, true), new TableCell(StatusImageView))
                }
            };

            var logLabel = new Label() 
            { 
                Text = "Log: ", 
                VerticalAlignment = VerticalAlignment.Center 
            };
            var logTable = new TableLayout()
            {
                Spacing = new Size(3, 3),
                Rows =
                {
                    new TableRow(new TableCell(logLabel, true), new TableCell(OpenDetailLogButton))
                }
            };
            Content = new TableLayout
            {
                Padding = 5,
                Spacing = new Size(3, 3),
                Rows =
                {
                    new TableRow(infoTable),
                    new TableRow(new TableCell(ExePathControl)),
                    new TableRow(new TableCell(TabControl, true)),
                    new TableRow(new TableCell(logTable)),
                    new TableRow(new TableCell(LogTextArea, true)) { ScaleHeight = true }
                }
            };

            Menu = new MenuBar
            {
                QuitItem = new Command((sender, e) => Application.Instance.Quit())
                {
                    MenuText = "Quit",
                    Shortcut = Application.Instance.CommonModifier | Keys.Q
                },

                AboutItem = new Command((sender, e) => ShowAboutDialog())
                {
                    MenuText = "About"
                }
            };
        }

        public ILogger Logger { get; set; } = NullLogger.Instance;

        public ImageView StatusImageView { get; set; }

        public Label InfoLabel { get; set; }

        public FilePathControl ExePathControl { get; set; }

        public Button OpenDetailLogButton { get; set; }

        public TabControl TabControl { get; set; }

        public InstallOptionsPanel InstallPanel { get; set; }

        public UninstallOptionsPanel UninstallPanel { get; set; }

        public RichTextArea LogTextArea { get; set; }

        public UIHost Host { get; set; }

        public EtgPlatformSpecifics PlatformSpecifics { get; set; } = EtgPlatformSpecifics.Current;

        public async void Startup()
        {
            try
            {
                Host.Initialize();
                if (Host.DiscoverApplication())
                {
                    ExePathControl.FilePathTextBox.Text = Host.Path.RawPath;
                    await UpdateUIInfo();
                }
                else
                {
                    InfoLabel.Text = "Enter the gungeon not found!";
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Error in Startup");
                LogTextArea.Append($"!!! Error: {e}", true);
            }
        }

        private async void ExePathChanged(object sender, EventArgs e)
        {
            try
            {
                bool exists = Host.SetApplicationPathFromInput(ExePathControl.FilePathTextBox.Text);
                if (exists)
                {
                    await UpdateUIInfo();
                }
                else
                {
                    InfoLabel.Text = "Enter the gungeon not found!";
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error processing path change");
                LogTextArea.Append($"!!! Error: {ex}", true);
            }
        }

        private async Task UpdateUIInfo()
        {
            var info = await Task.Run(async () => await Host.GetInfo());
            var builder = new StringBuilder();
            if (info.Found)
            {
                builder.AppendLine($"Installed game version: {info.InstalledGameVersion}");
                builder.AppendLine($"Mod the Gungeon: {(info.IsModded ? "installed" : "not installed")}");
                if (info.IsModded)
                {
                    builder.AppendLine($"MtG version: {info.InstalledModVersion}");
                }
            }
            else
            {
                builder.AppendLine("Enter the gungeon not found!");
            }

            InfoLabel.Text = builder.ToString();
        }

        private async void OpenFileClicked(object sender, EventArgs e)
        {
            // TODO: Mac might need to use a SelectFolderDialog for the app bundle.
            using var dialog = new OpenFileDialog()
            {
                MultiSelect = false,
                Title = "Select Executable for Enter the Gungeon"
            };

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                dialog.Filters.Add(new FileFilter()
                {
                    Name = "Executables",
                    Extensions = new[] { "*.exe" }
                });
            }

            dialog.SetDereferenceLinks(false);

            var result = dialog.ShowDialog(this);
            if (result == DialogResult.Ok)
            {
                string filePath = dialog.FileName;
                string expectedFileName = PlatformSpecifics.ExecutableName;
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    string ext = Path.GetExtension(dialog.FileName);
                    if (string.Equals(ext, ".lnk", StringComparison.OrdinalIgnoreCase) || string.Equals(ext, ".url", StringComparison.OrdinalIgnoreCase))
                    {
                        string text = $"You have selected a link. This cannot be used by the installer. Please select the executable file for Enter the Gungeon ({expectedFileName}).";
                        MessageBox.Show(this, text, "Link file selected", MessageBoxButtons.OK, MessageBoxType.Warning);
                        return;
                    }
                }

                string fileName = Path.GetFileName(filePath);
                if (expectedFileName != fileName)
                {
                    string text = $"The name of the file you have selected ({fileName}) does not match the expected file name for the platform ({expectedFileName}). Proceed?";
                    var questionResult = MessageBox.Show(this, text, "Unexpected file name", MessageBoxButtons.YesNo, MessageBoxType.Question, MessageBoxDefaultButton.No);
                    if (questionResult != DialogResult.Yes)
                    {
                        return;
                    }
                }

                ExePathControl.FilePathTextBox.Text = filePath;
                Host.SetApplicationPathFromInput(filePath);
                await UpdateUIInfo();
            }
        }

        private async void InstallClicked(object sender, EventArgs e)
        {
            try
            {
                ClearOutput();
                DisableInputs();

                var sourceOption = InstallPanel.SourceComboBox.SelectedValue;
                string zipPath = sourceOption == SourceOption.Zip ? InstallPanel.ZipFilePathControl.FilePathTextBox.Text : null;
                var parameters = new InstallParameters()
                {
                    AutoRun = InstallPanel.AutoRunCheckBox.Checked ?? false,
                    ForceClose = InstallPanel.ForceCloseCheckBox.Checked ?? false,
                    Source = sourceOption,
                    PackageZipFilePath = zipPath
                };

                if (sourceOption == SourceOption.Zip && string.IsNullOrEmpty(parameters.PackageZipFilePath))
                {
                    MessageBox.Show("Select a zip file to use the Zip source option");
                    return;
                }

                await Task.Run(async () => await Host.Install(parameters));
                await UpdateUIInfo();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error in InstallClicked");
                LogTextArea.Append($"!!! Error: {ex}", true);
            }
            finally
            {
                EnableInputs();
                UpdateHostStatus();
            }
        }

        private async void UninstallClicked(object sender, EventArgs e)
        {
            try
            {
                ClearOutput();
                DisableInputs();

                var parameters = new UninstallParameters()
                {
                    ForceClose = UninstallPanel.ForceCloseCheckBox.Checked ?? false
                };

                await Task.Run(async () => await Host.Uninstall(parameters));
                await UpdateUIInfo();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error in UninstallClicked");
                LogTextArea.Append($"!!! Error: {ex}", true);
            }
            finally
            {
                EnableInputs();
                UpdateHostStatus();
            }
        }

        private void ClearOutput()
        {
            StatusImageView.Image = null;
            LogTextArea.Text = string.Empty;
        }

        private void DisableInputs()
        {
            ExePathControl.Enabled = false;
            TabControl.Enabled = false;
        }

        private void EnableInputs()
        {
            ExePathControl.Enabled = true;
            TabControl.Enabled = true;
        }

        private void UpdateHostStatus()
        {
            switch (Host.Status)
            {
                case HostStatus.Success:
                    StatusImageView.Image = Bitmap.FromResource("Etgmod.Installer.UI.Assets.icons.finished.gif");
                    break;
                case HostStatus.Failure:
                    StatusImageView.Image = Bitmap.FromResource("Etgmod.Installer.UI.Assets.icons.error.gif");
                    break;
                case HostStatus.None:
                default:
                    StatusImageView.Image = null;
                    break;
            }
        }

        private void ShowAboutDialog()
        {
            using var dialog = new AboutDialog();
            dialog.Logo = Icon.FromResource(@"Etgmod.Installer.UI.Assets.icons.icon.png");
            dialog.Title = "ETGMod Installer";
            dialog.ProgramName = "ETGMod Installer: Reloaded";
            dialog.ProgramDescription = "A new and improved installer for Mod the Gungeon";
            dialog.Version = ThisAssembly.AssemblyInformationalVersion;
            dialog.License = "MIT License\n\nCopyright (c) 2021 Scion of Memory";
            dialog.Website = new Uri("https://gitlab.com/scionofmemory/etgmod-installer-reloaded");

            dialog.ShowDialog(this);
        }

        private void OpenDetailLogClicked(object sender, EventArgs e)
        {
            string logPath = ((EtgModInstallerApp)Application.Instance).LogFilePath;
            if (string.IsNullOrEmpty(logPath))
            {
                MessageBox.Show("Log file path was not set.");
                return;
            }

            if (!File.Exists(logPath))
            {
                MessageBox.Show($"Log file path was found '{logPath}'.");
                return;
            }

            var startInfo = new ProcessStartInfo()
            {
                FileName = logPath,
                UseShellExecute = true
            };
            Process.Start(startInfo);
        }
    }
}
