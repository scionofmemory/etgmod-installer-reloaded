using System;
using Etgmod.Installer.Core;
using Eto.Drawing;
using Eto.Forms;

namespace Etgmod.Installer.UI
{
    public partial class InstallOptionsPanel : Panel
    {
        public InstallOptionsPanel()
        {
            AutoRunCheckBox = new CheckBox()
            {
                Text = "Auto-run",
                ToolTip = "Run Enter the Gungeon automatically when installation finishes successfully"
            };

            ForceCloseCheckBox = new CheckBox()
            {
                Text = "Force Close",
                ToolTip = "Force any running Enter the Gungeon processes to close before running the operation."
            };

            SourceLabel = new Label()
            {
                Text = "Source",
                VerticalAlignment = VerticalAlignment.Center
            };

            // TODO: Tool tips?
            // Offline tooltip = "Skip any download steps and used cached version of Mod the Gungeon."
            SourceComboBox = new EnumDropDown<SourceOption>()
            {
                GetText = item =>
                {
                    return item switch 
                    {
                        SourceOption.AutomaticDownload => "Automatic download (recommended)",
                        SourceOption.Offline => "Offline cache",
                        SourceOption.Zip => "Zip",
                        _ => item.ToString()
                    };
                },
                SelectedValue = SourceOption.AutomaticDownload
            };

            ZipFilePathControl = new FilePathControl();

            InstallButton = new Button()
            {
                Text = "Install",
                ToolTip = "Install Mod the Gungeon"
            };

            var sourceLayout = new StackLayout()
            {
                Spacing =3,
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Center,
                Items = 
                {
                    SourceLabel, 
                    SourceComboBox
                }
            };

            var innerTable = new TableLayout()
            {
                Spacing = new Size(3, 3),
                Rows =
                {
                    new TableRow(new TableCell(ForceCloseCheckBox, false)),
                    new TableRow(new TableCell(AutoRunCheckBox, false)),
                    new TableRow(new TableCell(sourceLayout, false)),
                    new TableRow(new TableCell(ZipFilePathControl)),
                }
            };

            var outerTable = new TableLayout()
            {
                Padding = new Padding(5),
                Rows =
                {
                    new TableRow(new TableCell(innerTable, true)) { ScaleHeight = true },
                    new TableRow(new TableCell(null, true), new TableCell(InstallButton)),
                }
            };

            Content = outerTable;

            ZipFilePathControl.OpenFileButton.Click += ZipOpenFileClicked;
            SourceComboBox.SelectedIndexChanged += SourceSelectedIndexChanged;
            UpdateZipOptionsVisibility();
        }

        public CheckBox AutoRunCheckBox { get; set; }

        public CheckBox ForceCloseCheckBox { get; set; }

        public Label SourceLabel { get; set; }

        public EnumDropDown<SourceOption> SourceComboBox { get; set; }

        public FilePathControl ZipFilePathControl { get; set; }

        public Button InstallButton { get; set; }

        private void SourceSelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateZipOptionsVisibility();
        }

        private void UpdateZipOptionsVisibility()
        {
            bool showZipOptions = SourceComboBox.SelectedValue == SourceOption.Zip;
            ZipFilePathControl.Visible = showZipOptions;
            ZipFilePathControl.Enabled = showZipOptions;
        }

        private void ZipOpenFileClicked(object sender, EventArgs e)
        {
            using var dialog = new OpenFileDialog()
            {
                MultiSelect = false,
                Title = "Select ETGMOD.zip",
                Filters = 
                {
                    new FileFilter()
                    {
                        Name = "Zip",
                        Extensions = new[] { "*.zip" }
                    }
                }
            };

            var result = dialog.ShowDialog(this);
            if (result == DialogResult.Ok)
            {
                ZipFilePathControl.FilePathTextBox.Text = dialog.FileName;
            }
        }
    }
}
