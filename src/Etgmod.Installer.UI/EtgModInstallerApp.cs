using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Etgmod.Installer.Core;
using Eto;
using Eto.Forms;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Etgmod.Installer.UI
{
    public class EtgModInstallerApp : Application
    {
        private IServiceProvider _serviceProvider;
        private ILogger _logger;
        private InstallerWindow _window;

        public EtgModInstallerApp(string platform)
            : base(platform)
        {
            _window = new InstallerWindow();
            var services = new ServiceCollection()
                .AddLogging(lb => lb.AddSerilog(dispose: true))
                .AddEtgmodInstallerServices()
                .AddSingleton<IReporter, InstallerWindowReporting>()
                .AddSingleton<InstallerWindow>(_window)
                .AddSingleton<UIHost>();
            _serviceProvider = services.BuildServiceProvider();
            _window.Logger = _serviceProvider.GetRequiredService<ILogger<InstallerWindow>>();
            MainForm = _window;
        }

        public string LogFilePath { get; private set; }

        protected override void OnTerminating(CancelEventArgs e)
        {
            Log.CloseAndFlush();
        }

        protected override void OnInitialized(EventArgs e)
        {
            SetupLogging();
            _logger = _serviceProvider.GetRequiredService<ILogger<EtgModInstallerApp>>();
            _window.Host = _serviceProvider.GetRequiredService<UIHost>();
            MainForm.Show();
            _window.Startup();
        }

        protected override void OnUnhandledException(Eto.UnhandledExceptionEventArgs e)
        {
            if (_logger != null)
            {
                if (e.ExceptionObject is Exception exception)
                {
                    _logger.LogError(exception, "An unhandled exception occurred");
                }
                else
                {
                    _logger.LogError("An error occurred: {ExObject}", e.ExceptionObject);
                }
            }

            base.OnUnhandledException(e);
        }

        private void SetupLogging()
        {
            string logFilePath = Path.Combine(AppContext.BaseDirectory, "etgmod_installer.log");

            try
            {
                if (File.Exists(logFilePath))
                {
                    File.Delete(logFilePath);
                }
            }
            catch (Exception)
            {
            }

            bool verbose = Environment.GetCommandLineArgs().Any(arg => 
                string.Equals(arg, "-v", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(arg, "--verbose", StringComparison.OrdinalIgnoreCase));

            LogEventLevel level = verbose ? LogEventLevel.Verbose : LogEventLevel.Information;
            
            string outputTemplate = "{Timestamp:HH:mm:ss.fff} [{Level:u3}] [{SourceContext}] {Message}{NewLine}{Exception}";
            var config = new LoggerConfiguration()
              .MinimumLevel.Is(level)
              .Enrich.FromLogContext()
              .WriteTo.File(logFilePath, outputTemplate: outputTemplate, fileSizeLimitBytes: 10_000_000);
            var logger = config.CreateLogger();
            LogFilePath = logFilePath;
            Log.Logger = logger;
        }
    }
}
