﻿using System;
using Etgmod.Installer.Core;
using Eto.Drawing;
using Eto.Forms;

namespace Etgmod.Installer.UI
{
    public class InstallerWindowReporting : IReporter
    {
        private readonly SolidBrush ImportantBrush = new SolidBrush(Color.FromArgb(255, 63, 63, 91));

        private readonly InstallerWindow _window;

        public InstallerWindowReporting(InstallerWindow window)
        {
            _window = window;
        }

        public void ImportantProgressMessage(string message)
        {
        }

        public void InitProgress(string message, int max)
        {
            WriteLine($"Starting {message}, parts:{max}");
        }

        public void EndProgress(string message)
        {
            WriteLine($"Finished {message}");
        }

        public void Log(string message)
        {
            WriteLine(message);
        }

        public void OnActivity()
        {
        }

        public void SetProgress(int value)
        {
        }

        public void SetProgress(string message, int value)
        {
        }

        private void WriteLine(string message)
        {
            Application.Instance.Invoke(() => {
                _window.LogTextArea.Append(message, false);
                _window.LogTextArea.Append(Environment.NewLine, true);
            });
        }
    }
}
