﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Eto.Forms;

namespace Etgmod.Installer.UI
{
    public static class WpfExtensions
    {
        public static void SetDereferenceLinks(this OpenFileDialog dialog, bool value)
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return;
            }

            var control = dialog.ControlObject;
            var prop = control.GetType().GetProperty("DereferenceLinks", BindingFlags.Public | BindingFlags.Instance);
            if (prop == null)
            {
#if DEBUG
                throw new Exception("Failed to get property OpenFileDialog's Control: DereferenceLinks");
#else
                return;
#endif
            }

            prop.SetValue(control, value);
        }
    }
}
