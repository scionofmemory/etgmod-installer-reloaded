A completely rewritten Mod the Gungeon installer with support for Windows, Linux, and MacOS.

# Features

- Runs on all supported Enter the Gungeon OS platforms.
- Automatic detection of Enter the Gungeon, including platforms Steam and Epic Games.
- Install Mod the Gungeon.
- Uninstall Mod the Gungeon.

NOTE: Installations from Xbox Game Pass/Microsoft Store are not supported.

# Usage

CLI commands and options:

Note help options, (-?,-h,--help) can be used for any command to show help.

- info - displays information about the current installation. Options:
>
    -a|--app <APP>  Explicitly specify the path to the Enter the Gungeon executable/application bundle.
    -l|--log        Output to console as a log.
    -v|--verbose    Enables verbose logging.

- install - installs Mod the Gungeon. Options:
>
    -a|--app <APP>  No automatic location of EtG. Explicitly specify the path to the
                    Enter the Gungeon executable/application bundle.
    -l|--log        Output to console as a log.
    -v|--verbose    Enables verbose logging.
    --autorun       Automatically run EtG after install finishes.
    --force-close   Force EtG to close before performing any changes.
    --offline       Skips download steps. Requires ETGMod.zip is present in cache directory. (Exclusive with --zip)
    --zip <FILE>    Specify path to ETGMOD.zip file. Skips any download. (Exclusive with --offline)

- uninstall - uninstall Mod the Gungeon. Options:
>
    -a|--app <APP>  No automatic location of EtG. Specifies the path to the Enter the Gungeon executable/application bundle.
    -l|--log        Output to console as a log.
    -v|--verbose    Enables verbose logging.
    --force-close   Force EtG to close before performing any changes.

UI command line flags:
>
    -v|--verbose    Verbose output to log file.

# Development

## Prerequisites

- .NET 5 - https://dotnet.microsoft.com/download/dotnet/5.0
- IDE such as Visual Studio or Visual Studio Code is helpful.
- Windows is required to build the Wpf UI project.

## Projects

- Etgmod.Installer.Core - The core logic for the installer
- Etgmod.Installer.UI - Common UI controls and logic
- Etgmod.Installer.Gtk - UI application targeted for Linux
- Etgmod.Installer.Mac - UI application targeted for MacOS
- Etgmod.Installer.Wpf - UI application targeted for Windows
- Etgmod.Installer - A CLI that can be compiled for all platforms